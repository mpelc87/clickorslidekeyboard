CREDITS

Java Android source files source:
Source files of The Android Open Source Project are used in this application.
Licence: Apache License, Version 2.0, http://www.apache.org/licenses/LICENSE-2.0.

Icons source:
Icons from https://material.io/icons/ are used in this application.
Licence: Apache License, Version 2.0, http://www.apache.org/licenses/LICENSE-2.0.

Suggestion words source:
Words from https://github.com/mkpelc/frequencywordslists are used in this application.
Licence: CC BY-SA 3.0, http://creativecommons.org/licenses/by-sa/3.0/.
The above work is created from word lists - "Frequency Word Lists" available on https://invokeit.wordpress.com/frequency-word-lists/ which were created by Hermit Dave on CC BY-SA 3.0 license.