package com.gigantbuttonskeyboard.suggestions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardLanguages;

import android.content.Context;
import android.widget.Toast;

public class SuggestionsMap {
	private Map<Character, ArrayList<String>> mSuggestionsNoAccMap;
	private Map<Character, ArrayList<String>> mSuggestionsWithAccMap;
	Context mContext;
	
	public SuggestionsMap(Map<Character,ArrayList<String>> suggestionsNoAccMap, Map<Character, ArrayList<String>> suggestionsWithAccMap, Context context) { 
		mSuggestionsNoAccMap = suggestionsNoAccMap;
		mSuggestionsWithAccMap = suggestionsWithAccMap;
		mContext = context;
	}
	
	public Map<Character,ArrayList<String>> getSuggestionsMap() {
		return mSuggestionsNoAccMap;
	}
	
	public ArrayList<String> findSuggestions(String wordPart, int amount, int depth, boolean withAcc, boolean capsLock, KeyboardLanguages currentLang) {
		ArrayList<String> suggestions = new ArrayList<String>();
		Set<String> foundSuggestions = findSuggestionsWords(wordPart, amount, depth, withAcc, capsLock, currentLang);
		if(foundSuggestions != null) {
			suggestions.addAll(foundSuggestions);
		}

		return suggestions;
	}

	private Set<String> findSuggestionsWords(String wordPart, int amount, int depth, boolean withAcc, boolean isCapsLockActive, KeyboardLanguages currentLang) {
		String wordPartNoAcc = SuggestionsHelper.stripAccentFromString(wordPart);
		Character firstLetter = wordPartNoAcc.toLowerCase().charAt(0);
		ArrayList<String> wordsNoAcc = mSuggestionsNoAccMap.get(firstLetter);
		ArrayList<String> wordsWithAcc = mSuggestionsWithAccMap.get(firstLetter);
		
		if(wordsNoAcc == null || wordsWithAcc == null) {
			return null;
		}
		
		if(currentLang == KeyboardLanguages.PL && wordsNoAcc.size() != wordsWithAcc.size()) {
			SuggestionsHelper.removeDictionaryFilesForLetter(mContext, firstLetter);
			SuggestionsHelper.copyResDictionaryFilesToInternalStorageForLetter(mContext, firstLetter);
			
			mSuggestionsWithAccMap.put(firstLetter, SuggestionsHelper.readDictionariesForLetter(firstLetter, mContext, true));
			mSuggestionsNoAccMap.put(firstLetter, SuggestionsHelper.readDictionariesForLetter(firstLetter, mContext, false));
			Toast.makeText(mContext, mContext.getString(R.string.suggestions_reloading_toast_message), Toast.LENGTH_LONG).show();
		}
		
		if(currentLang != KeyboardLanguages.PL) {
			return searchSuggestions(wordPart, wordsWithAcc, wordsWithAcc, amount, depth, isCapsLockActive);
		} 
		//languages other then English
		if(withAcc) {
			if(hasPolishAccentLetters(wordPart)) {
				return searchSuggestions(wordPart, wordsWithAcc, wordsWithAcc, amount, depth, isCapsLockActive);
			} 
			return searchSuggestions(wordPart, wordsNoAcc, wordsWithAcc, amount, depth, isCapsLockActive);
		} 	
		return searchSuggestions(wordPartNoAcc, wordsNoAcc, wordsNoAcc, amount, depth, isCapsLockActive);
	}
	
	private Set<String> searchSuggestions(String wordPart, ArrayList<String> listToSearchIn, ArrayList<String> listToGetFrom, int amount, int depth, boolean isCapsLockActive) {
		boolean isCapitalFirstLetter = Character.isUpperCase(wordPart.charAt(0));
		Set<String> suggestions = new LinkedHashSet<String>();
		Set<String> suggestionsDepthNotEnough = new LinkedHashSet<String>();
		
		String wordPartLowerCase = wordPart.toLowerCase();
		
		int wordPartLen = wordPartLowerCase.length();
		for(int i=0; i<listToSearchIn.size(); ++i) {
			if(listToSearchIn.get(i).startsWith(wordPartLowerCase) ) {
				if(listToSearchIn.get(i).length() >= wordPartLen + depth) {
					String foundWord = listToGetFrom.get(i); //null pointer exception
					if(foundWord != null) {
						suggestions.add(getSuggestionWithUpdatedLetters(foundWord, isCapitalFirstLetter, isCapsLockActive));
					}
				} else {
					String foundWord = listToGetFrom.get(i);
					if(foundWord != null) {
						suggestionsDepthNotEnough.add(getSuggestionWithUpdatedLetters(foundWord, isCapitalFirstLetter, isCapsLockActive));	
					}
				}
			}
			if(suggestions.size() == amount) {
				break;
			}
		}
		
		//not enough suggestions with good depth were found
		int index = suggestions.size();
		Iterator<String> iterator = suggestionsDepthNotEnough.iterator();
		while(iterator.hasNext() && index < amount) {
			suggestions.add(iterator.next());
		}
		
		return suggestions;
	}
	
	private String getSuggestionWithUpdatedLetters(String suggestion, boolean isCapitalFirstLetter, boolean isCapsLockActive) {
		if(isCapsLockActive) {
			return suggestion.toUpperCase();
		}
		if(isCapitalFirstLetter) {
			if(suggestion.length() == 1) {
				return suggestion.toUpperCase();
			}
			return Character.toUpperCase(suggestion.charAt(0)) + suggestion.substring(1);
		}
		return suggestion;
	}
	
	private boolean hasPolishAccentLetters(String word) {
		for(char c : word.toLowerCase().toCharArray()) {
			int index =  c - 'a';
			if(index < 0 || index > 25) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isWordInSuggestions(String word) {
		String wordLowerCase = word.toLowerCase();
		Character firstLetter = SuggestionsHelper.stripAccentFromString(wordLowerCase).charAt(0);
		ArrayList<String> words = mSuggestionsWithAccMap.get(firstLetter);

		if(words != null && words.contains(wordLowerCase)) {
			return true;
		}
		return false;
	}
	
	public void addWordToSuggestions(String word) {
		String wordLowerCaseWithAcc = word.toLowerCase();
		String wordLowerCaseNoAcc = SuggestionsHelper.stripAccentFromString(wordLowerCaseWithAcc);
		Character firstLetter = wordLowerCaseNoAcc.charAt(0);
		
		ArrayList<String> wordsWithAcc = mSuggestionsWithAccMap.get(firstLetter);
		if(wordsWithAcc != null) {
			wordsWithAcc.add(wordLowerCaseWithAcc);
		}
		
		ArrayList<String> wordsNoAcc = mSuggestionsNoAccMap.get(firstLetter);
		if(wordsNoAcc != null) {
			wordsNoAcc.add(wordLowerCaseNoAcc);
		}
	}
	
	public void removeWordFromSuggestions(String word, KeyboardLanguages currentLang) {
		String wordLowerCaseWithAcc = word.toLowerCase();
		String wordLowerCaseNoAcc = SuggestionsHelper.stripAccentFromString(wordLowerCaseWithAcc);
		Character firstLetter = wordLowerCaseNoAcc.charAt(0);
		
		ArrayList<String> wordsWithAcc = mSuggestionsWithAccMap.get(firstLetter);
		ArrayList<String> wordsNoAcc = mSuggestionsNoAccMap.get(firstLetter);
		if(wordsWithAcc == null || wordsNoAcc == null) {
			return;
		}
		
		if(currentLang != KeyboardLanguages.PL) {
			wordsWithAcc.remove(wordLowerCaseWithAcc);
			wordsNoAcc.remove(wordLowerCaseNoAcc);
		} else {
			int indexOfWord = wordsWithAcc.indexOf(wordLowerCaseWithAcc);
			
			if(indexOfWord < 0 || indexOfWord >= wordsNoAcc.size()) {
				return;
			}

			wordsWithAcc.remove(indexOfWord);	
			wordsNoAcc.remove(indexOfWord);
		}
		
	}
}
