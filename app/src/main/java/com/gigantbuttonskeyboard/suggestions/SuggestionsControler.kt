package com.gigantbuttonskeyboard.suggestions

import android.content.Context
import android.os.AsyncTask
import android.preference.PreferenceManager
import android.util.Log
import com.gigantbuttonskeyboard.R

class SuggestionsController(private val context: Context) {
    internal var suggestionsReady = false
    internal var suggestionsLoadingAsyncTask: AsyncTask<Void?, Void?, Void?>? = null
    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var suggestionsMap: SuggestionsMap? = null
    internal var generationStop = false

    internal fun generateSuggestions() {
        suggestionsReady = false
        if (suggestionsLoadingAsyncTask == null) {

            object : AsyncTask<Void?, Void?, Void?>() {
                override fun doInBackground(vararg params: Void?): Void? {
                    generationStop = false
                    val startTime = System.currentTimeMillis()
                    Log.d(TAG, "Start generating suggestions")
                    val suggestionsNoAccMap = readDictionaries(context, false)
                    val suggestionsWithAccMap = readDictionaries(context, true)
                    //TODO: add generation for polish accents here
                    if (suggestionsNoAccMap != null && suggestionsWithAccMap != null && sharedPreferences!!.getBoolean(
                            context.getString(R.string.prefer_suggestions_functionality),
                            true
                        ) && !generationStop
                    ) {
                        suggestionsReady = true
                        suggestionsMap =
                            SuggestionsMap(suggestionsNoAccMap, suggestionsWithAccMap, context)
                        Log.d(
                            TAG,
                            "Suggestions generated in ${System.currentTimeMillis() - startTime} ms"
                        )
                    } else {
                        suggestionsMap = null
                        System.gc()
                    }
                    suggestionsLoadingAsyncTask = null
                    return null
                }
            }.also {
                it.execute()
                suggestionsLoadingAsyncTask = it
            }
        }
    }

    fun readDictionaries(context: Context?, withAcc: Boolean): HashMap<Char, ArrayList<String>>? {
        val wordListsMap = HashMap<Char, ArrayList<String>>()
        val dictionariesMapIterator: Iterator<Map.Entry<Char, String>>
        dictionariesMapIterator = if (withAcc) {
            SuggestionsHelper.storageDictionariesWithAccMap.entries.iterator()
        } else {
            SuggestionsHelper.storageDictionariesNoAccMap.entries.iterator()
        }
        while (dictionariesMapIterator.hasNext() && !generationStop) {
            val (key) = dictionariesMapIterator.next()
            val wordList = SuggestionsHelper.readDictionariesForLetter(key, context, withAcc)
            wordListsMap[key] = wordList
        }
        return wordListsMap
    }

    companion object {
        private const val TAG = "SuggestionsController"
    }
}