package com.gigantbuttonskeyboard.suggestions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.SoftKeyboard;
import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardLanguages;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

public class SuggestionsHelper {
	 
	static final String userDictionaryFileName = "user_dictionary_";
	
	static HashMap<Character, Integer> resDictionariesNoAccMap = new HashMap<Character, Integer>();
	static HashMap<Character, Integer> resDictionariesWithAccMap = new HashMap<Character, Integer>();
	static HashMap<Character, String> storageDictionariesNoAccMap = new HashMap<Character, String>();
	static HashMap<Character, String> storageDictionariesWithAccMap = new HashMap<Character, String>();

	private static void initDictionariesMap() {
		if(resDictionariesNoAccMap.isEmpty()) {
			resDictionariesNoAccMap.put('a', R.raw.dictionary_a_no_acc);
			resDictionariesNoAccMap.put('b', R.raw.dictionary_b_no_acc);
			resDictionariesNoAccMap.put('c', R.raw.dictionary_c_no_acc);
			resDictionariesNoAccMap.put('d', R.raw.dictionary_d_no_acc);
			resDictionariesNoAccMap.put('e', R.raw.dictionary_e_no_acc);
			resDictionariesNoAccMap.put('f', R.raw.dictionary_f_no_acc);
			resDictionariesNoAccMap.put('g', R.raw.dictionary_g_no_acc);
			resDictionariesNoAccMap.put('h', R.raw.dictionary_h_no_acc);
			resDictionariesNoAccMap.put('i', R.raw.dictionary_i_no_acc);
			resDictionariesNoAccMap.put('j', R.raw.dictionary_j_no_acc);
			resDictionariesNoAccMap.put('k', R.raw.dictionary_k_no_acc);
			resDictionariesNoAccMap.put('l', R.raw.dictionary_l_no_acc);
			resDictionariesNoAccMap.put('m', R.raw.dictionary_m_no_acc);
			resDictionariesNoAccMap.put('n', R.raw.dictionary_n_no_acc);
			resDictionariesNoAccMap.put('o', R.raw.dictionary_o_no_acc);
			resDictionariesNoAccMap.put('p', R.raw.dictionary_p_no_acc);
			resDictionariesNoAccMap.put('q', R.raw.dictionary_q_no_acc);
			resDictionariesNoAccMap.put('r', R.raw.dictionary_r_no_acc);
			resDictionariesNoAccMap.put('s', R.raw.dictionary_s_no_acc);
			resDictionariesNoAccMap.put('t', R.raw.dictionary_t_no_acc);
			resDictionariesNoAccMap.put('u', R.raw.dictionary_u_no_acc);
			resDictionariesNoAccMap.put('v', R.raw.dictionary_v_no_acc);
			resDictionariesNoAccMap.put('w', R.raw.dictionary_w_no_acc);
			resDictionariesNoAccMap.put('x', R.raw.dictionary_x_no_acc);
			resDictionariesNoAccMap.put('y', R.raw.dictionary_y_no_acc);
			resDictionariesNoAccMap.put('z', R.raw.dictionary_z_no_acc);
		}
		
		if(resDictionariesWithAccMap.isEmpty()) {
			resDictionariesWithAccMap.put('a', R.raw.dictionary_a);
			resDictionariesWithAccMap.put('b', R.raw.dictionary_b);
			resDictionariesWithAccMap.put('c', R.raw.dictionary_c);
			resDictionariesWithAccMap.put('d', R.raw.dictionary_d);
			resDictionariesWithAccMap.put('e', R.raw.dictionary_e);
			resDictionariesWithAccMap.put('f', R.raw.dictionary_f);
			resDictionariesWithAccMap.put('g', R.raw.dictionary_g);
			resDictionariesWithAccMap.put('h', R.raw.dictionary_h);
			resDictionariesWithAccMap.put('i', R.raw.dictionary_i);
			resDictionariesWithAccMap.put('j', R.raw.dictionary_j);
			resDictionariesWithAccMap.put('k', R.raw.dictionary_k);
			resDictionariesWithAccMap.put('l', R.raw.dictionary_l);
			resDictionariesWithAccMap.put('m', R.raw.dictionary_m);
			resDictionariesWithAccMap.put('n', R.raw.dictionary_n);
			resDictionariesWithAccMap.put('o', R.raw.dictionary_o);
			resDictionariesWithAccMap.put('p', R.raw.dictionary_p);
			resDictionariesWithAccMap.put('q', R.raw.dictionary_q);
			resDictionariesWithAccMap.put('r', R.raw.dictionary_r);
			resDictionariesWithAccMap.put('s', R.raw.dictionary_s);
			resDictionariesWithAccMap.put('t', R.raw.dictionary_t);
			resDictionariesWithAccMap.put('u', R.raw.dictionary_u);
			resDictionariesWithAccMap.put('v', R.raw.dictionary_v);
			resDictionariesWithAccMap.put('w', R.raw.dictionary_w);
			resDictionariesWithAccMap.put('x', R.raw.dictionary_x);
			resDictionariesWithAccMap.put('y', R.raw.dictionary_y);
			resDictionariesWithAccMap.put('z', R.raw.dictionary_z);
		}
	}
	
	public static void setUpDictionaries(Context context) {
		initDictionariesMap();
		Iterator<Entry<Character, Integer>> dictionariesMapIterator = resDictionariesNoAccMap.entrySet().iterator();
		String currentLangSuffix = "_" + context.getResources().getConfiguration().locale.getLanguage();
		try {
			while(dictionariesMapIterator.hasNext()) {
				Entry<Character, Integer> dictionaryLetterResIdPair = (Entry<Character, Integer>)dictionariesMapIterator.next();
				String storageFileName = userDictionaryFileName + dictionaryLetterResIdPair.getKey() + currentLangSuffix;
				if(!context.getFileStreamPath(storageFileName).exists()) {
					Integer resFileId = dictionaryLetterResIdPair.getValue();
					copyResDictionaryFileToInternalStorage(context, storageFileName, resFileId);
				}
			    storageDictionariesNoAccMap.put(dictionaryLetterResIdPair.getKey(), storageFileName);
			}
		}
		catch (IOException e) {
		}
		
		Iterator<Entry<Character, Integer>> dictionariesWithAccMapIterator = resDictionariesWithAccMap.entrySet().iterator();
		currentLangSuffix = "_" + context.getResources().getConfiguration().locale.getLanguage();
		try {
			while(dictionariesWithAccMapIterator.hasNext()) {
				Entry<Character, Integer> dictionaryLetterResIdPair = (Entry<Character, Integer>)dictionariesWithAccMapIterator.next();
				String storageFileName = userDictionaryFileName + dictionaryLetterResIdPair.getKey() + currentLangSuffix + "with_acc";
				if(!context.getFileStreamPath(storageFileName).exists()) {
					Integer resFileId = dictionaryLetterResIdPair.getValue();
					copyResDictionaryFileToInternalStorage(context, storageFileName, resFileId);
				}
				storageDictionariesWithAccMap.put(dictionaryLetterResIdPair.getKey(), storageFileName);
			}
		}
		catch (IOException e) {
		}
	}
	
	public static void copyResDictionaryFilesToInternalStorageForLetter(Context context, char letter) {
		try {
			copyResDictionaryFileToInternalStorage(context, storageDictionariesWithAccMap.get(letter), resDictionariesWithAccMap.get(letter));
			copyResDictionaryFileToInternalStorage(context, storageDictionariesNoAccMap.get(letter), resDictionariesNoAccMap.get(letter));
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	private static void copyResDictionaryFileToInternalStorage(Context context, String storageFileName, Integer resFileId)
			throws FileNotFoundException, IOException {
		
		InputStream inputStream = context.getResources().openRawResource(resFileId);
		FileOutputStream outStream=context.openFileOutput(storageFileName, Context.MODE_PRIVATE);
		
		byte[] buff = new byte[1024];
		int read = 0;
		try {
		    while ((read = inputStream.read(buff)) > 0) {
		    	outStream.write(buff, 0, read);
		    }
		} finally {
			inputStream.close();
			outStream.close();
		}
	}
	
	public static void removeDictionaryFilesForLetter(Context context, char letter) {
		context.deleteFile(storageDictionariesWithAccMap.get(letter));
		context.deleteFile(storageDictionariesNoAccMap.get(letter));
	}
	
	public static void cleanInternalStorage(Context context) {
		String [] allFiles = context.fileList();
		for(int i=0; i<allFiles.length; ++i) {
			context.deleteFile(allFiles[i]);
		}
	}
	
	private static void cleanDictionariesForCurrentLocale(Context context) {
		String [] allFiles = context.fileList();
		String currentLang = context.getResources().getConfiguration().locale.getLanguage();
		for(int i=0; i<allFiles.length; ++i) {
			if(allFiles[i].contains(currentLang)) {
				context.deleteFile(allFiles[i]);
			}
		}
	}
	
	public static void resetAllDictionaries(Context context) {
		cleanInternalStorage(context);
		setUpDictionaries(context);
	}
	
	public static void resetCurrentDictionaries(Context context) {
		cleanDictionariesForCurrentLocale(context);
		setUpDictionaries(context);
	}
	
	public static ArrayList<String> readDictionariesForLetter(char letter, Context context, boolean withAcc) {
		ArrayList<String> wordList = new ArrayList<String>();
		try {
			InputStream inputStream;
			if(withAcc) {
				inputStream = context.openFileInput(storageDictionariesWithAccMap.get(letter));
			} else {
				inputStream = context.openFileInput(storageDictionariesNoAccMap.get(letter));
			}
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		    	wordList.add(line);
		    }
		    bufferedReader.close();
		}
		catch (IOException e) {
		} 
		
		return wordList;
	}

	public static void addNewWordToDictionary(Context context, String word) {
		String wordLowerCaseWithAcc = word.toLowerCase();
		String wordLowerCaseNoAcc = stripAccentFromString(wordLowerCaseWithAcc);
		try {
		    FileOutputStream dictionaryFileWithAcc, dictionaryFileNoAcc;

		    dictionaryFileWithAcc = context.openFileOutput(storageDictionariesWithAccMap.get(wordLowerCaseNoAcc.charAt(0)), Context.MODE_APPEND);
		    dictionaryFileNoAcc = context.openFileOutput(storageDictionariesNoAccMap.get(wordLowerCaseNoAcc.charAt(0)), Context.MODE_APPEND);
		    
			addWordToFile(wordLowerCaseWithAcc, dictionaryFileWithAcc);
			addWordToFile(wordLowerCaseNoAcc, dictionaryFileNoAcc);

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		
	}

	private static void addWordToFile(String word, FileOutputStream dictionaryFileName) throws IOException {
		OutputStreamWriter outputWriter = new OutputStreamWriter(dictionaryFileName);
		outputWriter.write(word);
		String separator = System.getProperty("line.separator");
		outputWriter.write(separator);
		outputWriter.close();
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void removeWordFromDictionaryInAsyncTask(final Context context, final String word, final KeyboardLanguages currentLang) {
		 AsyncTask<Void, Void, Void> removeWordAsyncTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... arg0) {
				removeWordFromDictionary(context, word, currentLang);
				return null;
			}
			 ;
		 };
		 
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT && Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			 removeWordAsyncTask.execute(); 
 		} else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
 			removeWordAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
 		}
	}

	public static void removeWordFromDictionary(Context context, String word, KeyboardLanguages currentLang) {
		String wordLowerCaseWithAcc = word.toLowerCase();
		String wordLowerCaseNoAcc = stripAccentFromString(wordLowerCaseWithAcc);
		try {
			String dictionaryWithAccFileName, dictionaryNoAccfileName;

			dictionaryWithAccFileName = storageDictionariesWithAccMap.get(wordLowerCaseNoAcc.charAt(0));
			dictionaryNoAccfileName = storageDictionariesNoAccMap.get(wordLowerCaseNoAcc.charAt(0));

			if(currentLang != KeyboardLanguages.PL) {
				removeWordFromFile(context, wordLowerCaseWithAcc, dictionaryWithAccFileName);
				removeWordFromFile(context, wordLowerCaseNoAcc, dictionaryNoAccfileName);
			} else {
				int removedWordLine = removeWordFromFile(context, wordLowerCaseWithAcc, dictionaryWithAccFileName);
				removeWordFromFileByLineNumber(context, removedWordLine, dictionaryNoAccfileName); //note: majac lll->��� i lll->lll usuniemy obydwa lll z 1 s�ownika i jedno lll z drugiego
			}
			
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		
	}

	private static void removeWordFromFileByLineNumber(Context context, int lineNumber, String dictionaryfileName) throws FileNotFoundException, IOException {
		InputStream inputStream = context.openFileInput(dictionaryfileName);
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

		FileOutputStream tempFileOut = context.openFileOutput(dictionaryfileName + "_temp", Context.MODE_PRIVATE);
		OutputStreamWriter outputWriter = new OutputStreamWriter(tempFileOut);

		int lineCounter = 0;
		String line;
		String separator = System.getProperty("line.separator");
		while ((line = bufferedReader.readLine()) != null) {
			if (lineCounter != lineNumber) {
				outputWriter.write(line);
				outputWriter.write(separator);
				++lineCounter;
			} else {
				++lineCounter;
			}
		}
		bufferedReader.close();
		outputWriter.close();

		File outFile = new File(context.getFilesDir() + "/" + dictionaryfileName + "_temp");
		if (!outFile.renameTo(new File(context.getFilesDir() + "/" + dictionaryfileName))) {

		}
	}
	
	private static int removeWordFromFile(Context context, String word, String dictionaryfileName) throws FileNotFoundException,
			IOException {
		InputStream inputStream = context.openFileInput(dictionaryfileName);
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		
		FileOutputStream tempFileOut=context.openFileOutput(dictionaryfileName+"_temp", Context.MODE_PRIVATE);
		OutputStreamWriter outputWriter=new OutputStreamWriter(tempFileOut);
		
		int lineCounter = 0;
		int wordLineNumber = 0;
		boolean found = false;
		String line;
		String separator = System.getProperty("line.separator");
		while ((line = bufferedReader.readLine()) != null) {
			if(!line.equals(word) || found == true) {
				outputWriter.write(line);
			    outputWriter.write(separator);
			    ++lineCounter;
			} else {
				wordLineNumber = lineCounter;
				++lineCounter;
				found = true;
			}
		}
		bufferedReader.close();
		outputWriter.close();
		
		File outFile = new File(context.getFilesDir() + "/" + dictionaryfileName+"_temp");
		if(!outFile.renameTo(new File(context.getFilesDir() + "/" + dictionaryfileName))) {

		}
		
		return wordLineNumber;
	}
		
	public static char stripAccentFromCharLowerCase(char wordChar) {
		char result = wordChar;
		switch (wordChar) {
			case 'ą':
				result = 'a';
				break;
			case 'ć':
				result = 'c';
				break;
			case 'ę':
				result = 'e';
				break;
			case 'ł':
				result = 'l';
				break;
			case 'ń':
				result = 'n';
				break;
			case 'ó':
				result = 'o';
				break;
			case 'ś':
				result = 's';
				break;
			case 'ż':
				result = 'z';
				break;
			case 'ź':
				result = 'z';
				break;
			case 'ä':
				result = 'a';
				break;
			case 'ë':
				result = 'e';
				break;
			case 'ö':
				result = 'o';
				break;
			case 'ü':
				result = 'u';
				break;
		}
		return result;
	}
	
	public static char stripAccentFromCharUpperCase(char wordChar) {
		char result = wordChar;
		switch (wordChar) {
			case 'Ą':
				result = 'A';
				break;
			case 'Ć':
				result = 'C';
				break;
			case 'Ę':
				result = 'E';
				break;
			case 'Ł':
				result = 'L';
				break;
			case 'Ń':
				result = 'N';
				break;
			case 'Ó':
				result = 'O';
				break;
			case 'Ś':
				result = 'S';
				break;
			case 'Ż':
				result = 'Z';
				break;
			case 'Ź':
				result = 'Z';
				break;
			case 'Ä':
				result = 'A';
				break;
			case 'Ë':
				result = 'E';
				break;
			case 'Ö':
				result = 'O';
				break;
			case 'Ü':
				result = 'U';
				break;
		}
		return result;
	}
	
	public static String stripAccentFromString(String word) { //TODO: make it more efficient
		char [] chars = word.toCharArray();
		for(int i = 0; i<chars.length; ++i) {
			if(chars[i] >= 243 || chars[i] <= 380) {
				chars[i] = SuggestionsHelper.stripAccentFromCharLowerCase(chars[i]);
				chars[i] = SuggestionsHelper.stripAccentFromCharUpperCase(chars[i]);
			}
		}
		return String.valueOf(chars);
	}
	
}
