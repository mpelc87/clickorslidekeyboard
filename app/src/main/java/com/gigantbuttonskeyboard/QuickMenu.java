package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.R;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class QuickMenu extends Activity {

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    LayoutInflater inflater = getLayoutInflater();
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.quick_menu, null);
	    
	    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
	    	Point size = new Point();
		    getWindowManager().getDefaultDisplay().getSize(size);
		    layout.setMinimumWidth((int)(size.x * 0.7));
	    } else {
	    	layout.setMinimumWidth((int)(getWindowManager().getDefaultDisplay().getWidth() * 0.7));
	    }
	    	    
	    builder.setView(layout);
	   
	    final AlertDialog alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.show();
	    
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				dialog.cancel();
                finish();
			}
		});
	    
	    View keyboardLayoutChange = alertDialog.findViewById(R.id.quick_menu_change_layout_linear_layout);
	    keyboardLayoutChange.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SoftKeyboard.Companion.getSoftKeyboard().handleKeyboardLayoutChange();
				alertDialog.dismiss();
			}
		});

		View keyboardSettings = alertDialog.findViewById(R.id.quick_menu_settings_linear_layout);
		keyboardSettings.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				SoftKeyboard.Companion.getSoftKeyboard().startSettingsActivity();
				alertDialog.dismiss();
			}
		});
	}
}