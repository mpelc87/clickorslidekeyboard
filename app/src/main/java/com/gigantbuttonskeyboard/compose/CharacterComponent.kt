package com.gigantbuttonskeyboard.compose

import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.gigantbuttonskeyboard.R
import com.gigantbuttonskeyboard.compose.theme.MyApplicationTheme

@Composable
fun CharacterComponent(drawableId: Int, contentDescription: String? = null) {
    Image(
        painter = painterResource(id = drawableId),
        contentDescription = contentDescription
    )
}

@Preview
@Composable
fun CharacterPreview() {
    MyApplicationTheme {
        CharacterComponent(R.drawable.letter_cap_a, "A")
    }
}