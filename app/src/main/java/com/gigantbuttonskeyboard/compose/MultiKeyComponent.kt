package com.gigantbuttonskeyboard.compose

import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.awaitDragOrCancellation
import androidx.compose.foundation.gestures.awaitEachGesture
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.awaitTouchSlopOrCancellation
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.PointerInputScope
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.tooling.preview.Preview
import com.gigantbuttonskeyboard.R

//Modifier.draggable(
//state: DraggableState,
//orientation: Orientation,
//enabled: Boolean,
//interactionSource: MutableInteractionSource?,
//startDragImmediately: Boolean,
//onDragStarted: suspend CoroutineScope.(startedPosition: Offset) -> Unit,
//onDragStopped: suspend CoroutineScope.(velocity: Float) -> Unit,
//reverseDirection: Boolean
//)
@Composable
fun MultiKeyComponent(
    rows: List<List<Int>>,
    onMoveCallback: (offsetX: Float, offsetY: Float) -> Unit,
    onUpCallback: (offsetX: Float, offsetY: Float, wasDrag: Boolean) -> Unit
) {
    Column(modifier = Modifier
        .background(Color.Black)
        .pointerInput(Unit) { handleGestures(onMoveCallback, onUpCallback) }) {
        rows.forEach { row ->
            Row {
                row.forEach { column ->
                    CharacterComponent(column)
                }
            }
        }
    }
}

private suspend fun PointerInputScope.handleGestures(
    onMoveCallback: (offsetX: Float, offsetY: Float) -> Unit,
    onUpCallback: (offsetX: Float, offsetY: Float, wasDrag: Boolean) -> Unit
) {
    awaitEachGesture {
        var startingX: Float
        var startingY: Float
        var wasDrag = false

        val down = awaitFirstDown().also {
            startingX = it.position.x
            startingY = it.position.y
        }
        var change = awaitTouchSlopOrCancellation(down.id) { change, _ ->
            change.consume()
        }
        while (change != null && change.pressed) {
            change = awaitDragOrCancellation(change.id)?.also {
                change = it
                it.consume()
                if (it.pressed) {
                    wasDrag = true
                    onMoveCallback(it.position.x - startingX, it.position.y - startingY)
                }
            }
        }
        val x = change?.position?.x ?: startingX
        val y = change?.position?.y ?: startingY
        onUpCallback(x - startingX, y - startingY, wasDrag)
    }
}

@Preview(
    backgroundColor = 0x000000, showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
fun MultiKeyComponentPreview() {
    MultiKeyComponent(
        listOf(
            listOf(R.drawable.letter_a, R.drawable.letter_b, R.drawable.letter_c),
            listOf(R.drawable.letter_d, R.drawable.letter_e, R.drawable.letter_f),
            listOf(R.drawable.letter_g, R.drawable.letter_h, R.drawable.letter_i)
        ),
        onMoveCallback = { offsetX, offsetY ->
            Log.i("preview", "move to offsetX: $offsetX, offsetY: $offsetY")
        },
        onUpCallback = { offsetX, offsetY, wasDrag ->
            Log.i("preview", "up in offsetX: $offsetX, offsetY: $offsetY, wasDrag: $wasDrag")
        }
    )
}