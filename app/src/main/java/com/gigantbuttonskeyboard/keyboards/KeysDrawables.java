package com.gigantbuttonskeyboard.keyboards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.LatinKey;
import com.gigantbuttonskeyboard.SoftKeyboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.preference.PreferenceManager;

public class KeysDrawables {
	
	protected static final HashMap<Integer, Integer> mCodesToDrawablesIds = createCodesToDrawablesIdsMapping();
	private static HashMap<Integer, Bitmap> mPortraitCodesToBitmaps = initBitmapDrawablesMapping(mCodesToDrawablesIds, Configuration.ORIENTATION_PORTRAIT);
	private static HashMap<Integer, Bitmap> mLandscapeCodesToBitmaps = initBitmapDrawablesMapping(mCodesToDrawablesIds, Configuration.ORIENTATION_LANDSCAPE);
	protected static final ArrayList<Integer> specialKeyCodes = initSpecialKeyCodes();
	
	private static final int LETTER_SHIFT_MARGIN = 3;
	private static final int CODE_OFFSET_FOR_CAPTAL_LETTERS = 32;
	private static final int CODES_PER_KEY = 9;
	
	public static  Map<Integer, Integer> getCodesToDrawablesIds() {
		return mCodesToDrawablesIds;
	}
	
	private static ArrayList<Integer> initSpecialKeyCodes() {
		ArrayList<Integer> keyCodes = new ArrayList<Integer>();
		keyCodes.add(-1);
		keyCodes.add(-2);
		keyCodes.add(-22);
		keyCodes.add(32);
		keyCodes.add(-5);
		keyCodes.add(10);
		keyCodes.add(-101);
		keyCodes.add(-102);
		keyCodes.add(-103);
		keyCodes.add(-104);
		keyCodes.add(-105);
		
		return keyCodes;
	}
	
	//Run on class creation
	private static HashMap<Integer, Bitmap> initBitmapDrawablesMapping(HashMap<Integer, Integer> codesToDrawablesIds, int orientation) {
		float lowerCaseletterSize = 1f;
		float upperCaseletterAndSymbolsSize = 1f;
		
		HashMap<Integer, Bitmap> codesToBitmapDrawables = new HashMap<Integer, Bitmap>();
		Set<Integer> keys = codesToDrawablesIds.keySet();
		for(Integer keyCode : keys) {
			float signSize = upperCaseletterAndSymbolsSize;
			if(isLowerCaseLetter(keyCode) == true) {
				signSize = lowerCaseletterSize;
			} 
			codesToBitmapDrawables.put(keyCode, createBitmapDrawable(codesToDrawablesIds.get(keyCode), signSize, orientation));
		}
		return codesToBitmapDrawables;
	}
	
	//Run on class creation
	private static boolean isLowerCaseLetter(int keyCode) {
		ArrayList<Integer> accLowLetter = new ArrayList<Integer>(Arrays.asList(223, 228, 246, 252, 261, 263, 281,322, 324, 243, 347, 378, 380));
		
		if(keyCode >= 97 && keyCode <= 122) { //small letters
			return true;
		} else if((keyCode >= 243 && keyCode <= 380) && accLowLetter.contains(keyCode)) { //range for polish accent letters
			return true; 
		} else {
			return false;
		}
	}
	
	//Run on class creation
	@SuppressLint("NewApi")
	private static Bitmap createBitmapDrawable(int drawableResId, float signSize, int orientation) {
		Context context = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext();
		Resources res = context.getResources();
		
		float defaultMultiKeyHeight = 0;
		if(orientation == Configuration.ORIENTATION_PORTRAIT) {
			defaultMultiKeyHeight = Math.min(res.getDisplayMetrics().heightPixels/6, (int)res.getDimension(R.dimen.multi_key_height));
		} else {
			defaultMultiKeyHeight = res.getDimension(R.dimen.multi_key_height);
		}
		
		int maxBitmapHeight = (int) ((defaultMultiKeyHeight/3)*1.1);
		
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		int keyHeight = (int)defaultMultiKeyHeight;
		if(orientation == Configuration.ORIENTATION_PORTRAIT) {
			keyHeight = sharedPreferences.getInt(context.getString(R.string.prefer_portrait_multikey_height), (int)defaultMultiKeyHeight);
		} else {
			keyHeight = sharedPreferences.getInt(context.getString(R.string.prefer_landscape_multikey_height), (int)defaultMultiKeyHeight);
		}
		int size = (int) (signSize * (Math.min(keyHeight/3, maxBitmapHeight)));
		Bitmap  resBitmaps = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, drawableResId), size, size, true);

		return resBitmaps;
	}
	
	//Run on class creation
    private static HashMap<Integer, Integer> createCodesToDrawablesIdsMapping() {
    	HashMap<Integer, Integer> codesDrawables = new HashMap<Integer, Integer>();
    	codesDrawables.put(0, R.drawable.empty);
    	
    	codesDrawables.put(119, R.drawable.letter_w);
    	codesDrawables.put(101, R.drawable.letter_e);
    	codesDrawables.put(114, R.drawable.letter_r);
    	codesDrawables.put(97, R.drawable.letter_a);
    	codesDrawables.put(113, R.drawable.letter_q);
    	codesDrawables.put(121, R.drawable.letter_y);
    	codesDrawables.put(116, R.drawable.letter_t);
    	codesDrawables.put(102, R.drawable.letter_f);
    	codesDrawables.put(103, R.drawable.letter_g);
    	codesDrawables.put(104, R.drawable.letter_h);
    	codesDrawables.put(111, R.drawable.letter_o);
    	codesDrawables.put(105, R.drawable.letter_i);
    	codesDrawables.put(117, R.drawable.letter_u);
    	codesDrawables.put(108, R.drawable.letter_l);
    	codesDrawables.put(112, R.drawable.letter_p);
    	codesDrawables.put(100, R.drawable.letter_d);
    	codesDrawables.put(115, R.drawable.letter_s);
    	codesDrawables.put(122, R.drawable.letter_z);
    	codesDrawables.put(120, R.drawable.letter_x);
    	codesDrawables.put(118, R.drawable.letter_v);
    	codesDrawables.put(99, R.drawable.letter_c);
    	codesDrawables.put(98, R.drawable.letter_b);
    	codesDrawables.put(106, R.drawable.letter_j);
    	codesDrawables.put(107, R.drawable.letter_k);
    	codesDrawables.put(110, R.drawable.letter_n);
    	codesDrawables.put(109, R.drawable.letter_m);
    	
    	codesDrawables.put(228, R.drawable.letter_a_umlaut);
    	codesDrawables.put(246, R.drawable.letter_o_umlaut);
    	codesDrawables.put(252, R.drawable.letter_u_umlaut);
    	codesDrawables.put(223, R.drawable.letter_s_szarfe);

    	codesDrawables.put(50, R.drawable.number_2);
    	codesDrawables.put(51, R.drawable.number_3);
    	codesDrawables.put(49, R.drawable.number_1);
    	codesDrawables.put(52, R.drawable.number_4);
    	codesDrawables.put(53, R.drawable.number_5);
    	codesDrawables.put(55, R.drawable.number_7);
    	codesDrawables.put(56, R.drawable.number_8);
    	codesDrawables.put(54, R.drawable.number_6);
    	codesDrawables.put(57, R.drawable.number_9);
    	codesDrawables.put(48, R.drawable.number_0);
    	codesDrawables.put(44, R.drawable.symbol_comma);
    	codesDrawables.put(46, R.drawable.symbol_dot);
    	codesDrawables.put(43, R.drawable.symbol_plus);
    	codesDrawables.put(37, R.drawable.symbol_percent);
    	codesDrawables.put(61, R.drawable.symbol_equal);
    	codesDrawables.put(42, R.drawable.symbol_star);
    	codesDrawables.put(45, R.drawable.symbol_minus);
    	codesDrawables.put(63, R.drawable.symbol_question_mark);
    	codesDrawables.put(33, R.drawable.symbol_exclamation_mark);
    	codesDrawables.put(64, R.drawable.symbol_at);
    	codesDrawables.put(38, R.drawable.symbol_ampersand);
    	codesDrawables.put(58, R.drawable.symbol_colon);
    	codesDrawables.put(59, R.drawable.symbol_semicolon);
    	codesDrawables.put(34, R.drawable.symbol_quotation_mark);
    	codesDrawables.put(39, R.drawable.symbol_aphostrope);
    	codesDrawables.put(47, R.drawable.symbol_slash);
    	codesDrawables.put(95, R.drawable.symbol_underscore); 
    	codesDrawables.put(40, R.drawable.symbol_open_parenthesis);
    	codesDrawables.put(41, R.drawable.symbol_close_parenthesis);
    	codesDrawables.put(92, R.drawable.symbol_back_slash);
    	codesDrawables.put(126, R.drawable.symbol_tilde);
    	codesDrawables.put(60, R.drawable.symbol_less_then);
    	codesDrawables.put(62, R.drawable.symbol_greater_then);
    	codesDrawables.put(124, R.drawable.symbol_pipe);
    	codesDrawables.put(96, R.drawable.symbol_grave_accent);
    	codesDrawables.put(91, R.drawable.symbol_open_bracket);
    	codesDrawables.put(93, R.drawable.symbol_close_bracket);
    	codesDrawables.put(35, R.drawable.symbol_hash);
    	codesDrawables.put(8230, R.drawable.symbol_triple_dot);
    	codesDrawables.put(123, R.drawable.symbol_open_brace);
    	codesDrawables.put(165, R.drawable.symbol_strange_y);
    	codesDrawables.put(125, R.drawable.symbol_close_brace);
    	codesDrawables.put(191, R.drawable.symbol_question_mark_upside_down);
    	codesDrawables.put(161, R.drawable.symbol_strange_i);
    	codesDrawables.put(36, R.drawable.symbol_dollar);
    	codesDrawables.put(163, R.drawable.symbol_pound);
    	codesDrawables.put(94, R.drawable.symbol_caret);
    	codesDrawables.put(8364, R.drawable.symbol_euro); //
    	codesDrawables.put(167, R.drawable.symbol_paragraph);
    	codesDrawables.put(8226, R.drawable.symbol_bullet);
    	codesDrawables.put(164, R.drawable.symbol_generic_currency);
    	codesDrawables.put(177, R.drawable.symbol_plus_minus);
    	codesDrawables.put(215, R.drawable.symbol_multiplication);
    	codesDrawables.put(247, R.drawable.symbol_division);
    	codesDrawables.put(176, R.drawable.symbol_degree);
    	codesDrawables.put(180, R.drawable.symbol_acute_accent);
    	codesDrawables.put(169, R.drawable.symbol_copyright);
    	codesDrawables.put(174, R.drawable.symbol_registered_trademark);

    	codesDrawables.put(87, R.drawable.letter_cap_w);
    	codesDrawables.put(69, R.drawable.letter_cap_e);
    	codesDrawables.put(82, R.drawable.letter_cap_r);
    	codesDrawables.put(65, R.drawable.letter_cap_a);
    	codesDrawables.put(81, R.drawable.letter_cap_q);
    	codesDrawables.put(89, R.drawable.letter_cap_y);
    	codesDrawables.put(84, R.drawable.letter_cap_t);
    	codesDrawables.put(70, R.drawable.letter_cap_f);
    	codesDrawables.put(71, R.drawable.letter_cap_g);
    	codesDrawables.put(72, R.drawable.letter_cap_h);
    	codesDrawables.put(79, R.drawable.letter_cap_o);
    	codesDrawables.put(73, R.drawable.letter_cap_i);
    	codesDrawables.put(85, R.drawable.letter_cap_u);
    	codesDrawables.put(76, R.drawable.letter_cap_l);
    	codesDrawables.put(80, R.drawable.letter_cap_p);
    	codesDrawables.put(68, R.drawable.letter_cap_d);
    	codesDrawables.put(83, R.drawable.letter_cap_s);
    	codesDrawables.put(90, R.drawable.letter_cap_z);
    	codesDrawables.put(88, R.drawable.letter_cap_x);
    	codesDrawables.put(86, R.drawable.letter_cap_v);
    	codesDrawables.put(67, R.drawable.letter_cap_c);
    	codesDrawables.put(66, R.drawable.letter_cap_b);
    	codesDrawables.put(74, R.drawable.letter_cap_j);
    	codesDrawables.put(75, R.drawable.letter_cap_k);
    	codesDrawables.put(78, R.drawable.letter_cap_n);
    	codesDrawables.put(77, R.drawable.letter_cap_m);

    	codesDrawables.put(261, R.drawable.letter_a_acc);
    	codesDrawables.put(263, R.drawable.letter_c_acc);
    	codesDrawables.put(281, R.drawable.letter_e_acc);
    	codesDrawables.put(322, R.drawable.letter_l_acc);
    	codesDrawables.put(324, R.drawable.letter_n_acc);
    	codesDrawables.put(243, R.drawable.letter_o_acc);
    	codesDrawables.put(347, R.drawable.letter_s_acc);
    	codesDrawables.put(380, R.drawable.letter_z_acc);
    	codesDrawables.put(378, R.drawable.letter_z_acc2);
    	codesDrawables.put(260, R.drawable.letter_cap_a_acc);
    	codesDrawables.put(262, R.drawable.letter_cap_c_acc);
    	codesDrawables.put(280, R.drawable.letter_cap_e_acc);
    	codesDrawables.put(321, R.drawable.letter_cap_l_acc);
    	codesDrawables.put(323, R.drawable.letter_cap_n_acc);
    	codesDrawables.put(211, R.drawable.letter_cap_o_acc);
    	codesDrawables.put(346, R.drawable.letter_cap_s_acc);
    	codesDrawables.put(379, R.drawable.letter_cap_z_acc);
    	codesDrawables.put(377, R.drawable.letter_cap_z_acc2);
    	
    	codesDrawables.put(196, R.drawable.letter_cap_a_umlaut);
    	codesDrawables.put(214, R.drawable.letter_cap_o_umlaut);
    	codesDrawables.put(220, R.drawable.letter_cap_u_umlaut);
    	codesDrawables.put(7838, R.drawable.letter_cap_s_szarfe);
    	
    	codesDrawables.put(-1, R.drawable.sym_keyboard_shift2);
    	codesDrawables.put(-2, R.drawable.sym_keyboard_123_2);
    	codesDrawables.put(-22, R.drawable.sym_keyboard_abc);
    	codesDrawables.put(32, R.drawable.sym_keyboard_space);
    	codesDrawables.put(-5, R.drawable.sym_keyboard_delete4);
    	codesDrawables.put(10, R.drawable.sym_keyboard_enter2);
    	codesDrawables.put(-101, R.drawable.accents);
    	codesDrawables.put(-102, R.drawable.sym_keyboard_change_layout);
//    	codesDrawables.put(-103, R.drawable.sym_keyboard_quick_menu);
    	codesDrawables.put(-103, R.drawable.sym_keyboard_settings);
    	codesDrawables.put(-104, R.drawable.sym_keyboard_change_layout);
    	codesDrawables.put(-105, R.drawable.emoji_face_smile);
    	
    	
    	return codesDrawables;
    }
	
	public static void reInitPortraitBitmapDrawablesMapping() {
		mPortraitCodesToBitmaps = initBitmapDrawablesMapping(mCodesToDrawablesIds, Configuration.ORIENTATION_PORTRAIT);
	}
	
	public static void reInitLandscapeBitmapDrawablesMapping() {
		mLandscapeCodesToBitmaps = initBitmapDrawablesMapping(mCodesToDrawablesIds, Configuration.ORIENTATION_LANDSCAPE);
	}
    
	@SuppressLint("NewApi")
	public static LayerDrawable createMultiKeyDrawable(LatinKey key, boolean isCapitalLetterKey, int orientation) {
		Resources res = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext().getResources();
		int [] keyCodes = new int[CODES_PER_KEY];
		System.arraycopy(key.codes, 0, keyCodes, 0, key.codes.length);
		
		if(isCapitalLetterKey) {
			modifyKeyCodesToUpperCase(keyCodes);
		}
		
		HashMap<Integer, Bitmap> codesToBitmaps = getCodesToBitmapsFromOrientation(orientation);
		
		Bitmap [] resBitmaps = new Bitmap[CODES_PER_KEY];
		for (int i=0; i<CODES_PER_KEY; ++i) {
			int keyCode = keyCodes[i];
			if (codesToBitmaps.containsKey(keyCode)) {
				resBitmaps[i] = codesToBitmaps.get(keyCodes[i]);
			} else {
				resBitmaps[i] = codesToBitmaps.get(0);
			}
		}
		Drawable [] layers = new Drawable[CODES_PER_KEY];
		for (int i=0; i<CODES_PER_KEY; ++i) {
			layers[i] = new BitmapDrawable(res, resBitmaps[i]);
		}

		return create9LayersDrawable(key, layers);
	}
	
	public static Drawable createRegularKeyDrawable(int keyCode, int orientation) {
		Resources res = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext().getResources();
		HashMap<Integer, Bitmap> codesToBitmaps = getCodesToBitmapsFromOrientation(orientation);
		
		Bitmap bitmap = null;
		if (codesToBitmaps.containsKey(keyCode)) {
			bitmap = codesToBitmaps.get(keyCode);
		} else {
			bitmap = codesToBitmaps.get(0);
		}
		
		Drawable keyDrawable = new BitmapDrawable(res, bitmap);
		return keyDrawable;
	}
	
	private static HashMap<Integer, Bitmap> getCodesToBitmapsFromOrientation(int orientation) {
		if(orientation == Configuration.ORIENTATION_PORTRAIT) {
			return mPortraitCodesToBitmaps;
		} 
		return mLandscapeCodesToBitmaps;
	}
	


	private static void modifyKeyCodesToUpperCase(int[] keyCodes) {
		for(int i=0; i<CODES_PER_KEY; ++i) {
			if(keyCodes[i] >= 97 && keyCodes[i] <= 122) { //change code only for letter
				keyCodes[i] = keyCodes[i] - CODE_OFFSET_FOR_CAPTAL_LETTERS;
			} else if(keyCodes[i] >= 223 && keyCodes[i] <= 380) { //range for polish and german accent letters
				int upperKeyCode = getGermanUpperCaseAccKeyCode(keyCodes[i]);
				if(upperKeyCode == 0) {
					upperKeyCode = getPolishUpperCaseAccKeyCode(keyCodes[i]);
				}
				keyCodes[i] = upperKeyCode;
			} 
		}
	}
	
	private static int getGermanUpperCaseAccKeyCode(int keyCode) {
		switch(keyCode) {
		case 228 :
			return 196;
		case 246 :
			return 214;
		case 252 :
			return 220;
		case 223 :
			return 7838;
		default :
			return 0;
		}
	}
	
	private static int getPolishUpperCaseAccKeyCode(int keyCode) {
		if(keyCode == 243) { //special case for ó
			return 211;
		} 
		return keyCode - 1;
	}

	private static LayerDrawable create9LayersDrawable(LatinKey key, Drawable[] layers) {
		LayerDrawable layerDrawable = new LayerDrawable(layers);
		int verticalOffset = getLetterVerticalShift(key);
		int horizontalOffset = getLetterHorizontalShift(key);
		//layer 0 does not need to be shifted
		layerDrawable.setLayerInset(1, 0, -verticalOffset, 0, verticalOffset); //up
		layerDrawable.setLayerInset(2, -horizontalOffset, 0, horizontalOffset, 0); //left
		layerDrawable.setLayerInset(3, 0, verticalOffset, 0, -verticalOffset); //down
		layerDrawable.setLayerInset(4, horizontalOffset, 0, -horizontalOffset, 0); //right
		
		layerDrawable.setLayerInset(5, -horizontalOffset, -verticalOffset, horizontalOffset, verticalOffset); 
		layerDrawable.setLayerInset(6, horizontalOffset, -verticalOffset, -horizontalOffset, verticalOffset); 
		layerDrawable.setLayerInset(7, -horizontalOffset, verticalOffset, horizontalOffset, -verticalOffset);  
		layerDrawable.setLayerInset(8, horizontalOffset, verticalOffset, -horizontalOffset, -verticalOffset);
		return layerDrawable;
	}
	
	private static LayerDrawable create7LayersDrawable(LatinKey key, Drawable[] layers) {
		LayerDrawable layerDrawable = new LayerDrawable(layers);
		int verticalOffset = getLetterVerticalShift(key);
		int horizontalOffset = getLetterHorizontalShift(key);
		//layer 0 does not need to be shifted
		layerDrawable.setLayerInset(1, 0, -verticalOffset, 0, verticalOffset); //up
		layerDrawable.setLayerInset(2, -horizontalOffset, 0, horizontalOffset, 0); //left
		layerDrawable.setLayerInset(3, 0, verticalOffset, 0, -verticalOffset); //down
		layerDrawable.setLayerInset(4, horizontalOffset, 0, -horizontalOffset, 0); //right
		
		layerDrawable.setLayerInset(5, -horizontalOffset, -verticalOffset, horizontalOffset, verticalOffset); 
		layerDrawable.setLayerInset(6, horizontalOffset, -verticalOffset, -horizontalOffset, verticalOffset); 
		layerDrawable.setLayerInset(7, -horizontalOffset, verticalOffset, horizontalOffset, -verticalOffset);  
		layerDrawable.setLayerInset(8, horizontalOffset, verticalOffset, -horizontalOffset, -verticalOffset);
		return layerDrawable;
	}
	
	private static int getLetterHorizontalShift(LatinKey key) {
		return key.width/3 - LETTER_SHIFT_MARGIN;
	}
	
	private static int getLetterVerticalShift(LatinKey key) {
		return key.height/3 - LETTER_SHIFT_MARGIN;
	}
}
