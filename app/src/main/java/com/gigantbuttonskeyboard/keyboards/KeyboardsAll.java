package com.gigantbuttonskeyboard.keyboards;

import com.gigantbuttonskeyboard.SoftKeyboard;
import com.gigantbuttonskeyboard.SoftKeyboard.CurrentKeyboardModes;

import android.content.res.Configuration;
import android.inputmethodservice.Keyboard;


public class KeyboardsAll {
	private PortraitKeyboardsGroup mPortraitsKeyboardsGroup;
	private LandscapeKeyboardsGroup mLandscapeKeyboardsGroup;
	
	public KeyboardsAll(SoftKeyboard.KeyboardModes portraitLayout, SoftKeyboard.KeyboardModes landscapeLayout) {
		mPortraitsKeyboardsGroup = new PortraitKeyboardsGroup(portraitLayout);
		mLandscapeKeyboardsGroup = new LandscapeKeyboardsGroup(landscapeLayout);
	}
	
	public void recreateKeyboardsGroupsOnOrientation(int orientation, SoftKeyboard.KeyboardModes portraitLayout, SoftKeyboard.KeyboardModes landscapeLayout) {
		if(orientation == Configuration.ORIENTATION_PORTRAIT) {
			mPortraitsKeyboardsGroup = new PortraitKeyboardsGroup(portraitLayout);
		} else {
			mLandscapeKeyboardsGroup = new LandscapeKeyboardsGroup(landscapeLayout);
		}
	}
	
		
	public KeyboardsGroup getPortraitKeyboardsGroup() {
		return mPortraitsKeyboardsGroup;
	}
	
	public KeyboardsGroup getLandscapeKeyboardsGroup() {
		return mLandscapeKeyboardsGroup;
	}
	
	public KeyboardsGroup getKeyboardsGroupFromOrientation(int orientation) {
		if(orientation == Configuration.ORIENTATION_PORTRAIT) {
			return mPortraitsKeyboardsGroup;
		}
		return mLandscapeKeyboardsGroup; 
	}
	
	public Keyboard getKeyboardFromModeAndOrientation(CurrentKeyboardModes keyboardMode, int orientation) {
		if(keyboardMode == CurrentKeyboardModes.LETTERS) {
			return getLettersKeyboardFromOrientation(orientation);
		} else if (keyboardMode == CurrentKeyboardModes.LETTERS_ACC) {
			return getLettersAccKeyboardFromOrientation(orientation);
		} else if (keyboardMode == CurrentKeyboardModes.SYMBOLS) {
			return getSymbolsKeyboardFromOrientation(orientation);
		} else {
			return getSymbolsShiftedKeyboardFromOrientation(orientation);
		}
	}
	
	public Keyboard getLettersKeyboardFromOrientation(int orientation) {
    	if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		return mPortraitsKeyboardsGroup.getLettersKeyboard();
    	}
    	return mLandscapeKeyboardsGroup.getLettersKeyboard();
    	
    }
    
	public Keyboard getSymbolsKeyboardFromOrientation(int orientation) {
    	if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		return mPortraitsKeyboardsGroup.getSymbolsKeyboard();
    	}
    	return mLandscapeKeyboardsGroup.getSymbolsKeyboard();
    }
    
	public Keyboard getSymbolsShiftedKeyboardFromOrientation(int orientation) {
    	if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		return mPortraitsKeyboardsGroup.getSymbolsShiftedKeyboard();
    	}
    	return mLandscapeKeyboardsGroup.getSymbolsShiftedKeyboard();
    }
    
	public Keyboard getLettersAccKeyboardFromOrientation(int orientation) {
		if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		return mPortraitsKeyboardsGroup.getLettersAccKeyboard();
    	}
    	return mLandscapeKeyboardsGroup.getLettersAccKeyboard();
    }
	
    public void reinitKeyboardsGroupToRegular(int orientation) {
    	if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		mPortraitsKeyboardsGroup.initKeyboardsGroupToRegular(); 
    	} else {
    		mLandscapeKeyboardsGroup.initKeyboardsGroupToRegular(); 
    	}
    }
    public void reinitKeyboardsGroupToMulti(int orientation) {
    	if (orientation == Configuration.ORIENTATION_PORTRAIT) {
    		mPortraitsKeyboardsGroup.initKeyboardsGroupToMulti(); 
    	} else {
    		mLandscapeKeyboardsGroup.initKeyboardsGroupToMulti(); 
    	}
    }
    
	public CurrentKeyboardModes getCurrentKeyboardModeFromOrientation(Keyboard currentKeyboard, int orientation) {
		if(currentKeyboard == getLettersKeyboardFromOrientation(orientation)) {
			return CurrentKeyboardModes.LETTERS;
		} else if (currentKeyboard == getLettersAccKeyboardFromOrientation(orientation)) {
			return CurrentKeyboardModes.LETTERS_ACC;
		} else if (currentKeyboard == getSymbolsKeyboardFromOrientation(orientation)) {
			return CurrentKeyboardModes.SYMBOLS;
		} else {
			return CurrentKeyboardModes.SYMBOLS_SHIFTED;
		}
	}
}
