package com.gigantbuttonskeyboard.keyboards

import android.inputmethodservice.Keyboard
import com.gigantbuttonskeyboard.SoftKeyboard
import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardModes

open class KeyboardsGroup {
    @JvmField
    protected var mContext =
        requireNotNull(SoftKeyboard.softKeyboard?.softKeyboardContext)
    var lettersKeyboard: Keyboard? = null
        protected set
    var lettersAccKeyboard: Keyboard? = null
        protected set
    var symbolsKeyboard: Keyboard? = null
        protected set
    var symbolsShiftedKeyboard: Keyboard? = null
        protected set
    var currentMode: KeyboardModes? = null
    protected var mDisplayHeightInPortrait: Int
    protected var mDisplayWidthInPortrait: Int
    private val mResources = mContext.resources

    protected fun initWithLayout(
        lettersLayout: Int,
        lettersAccLayout: Int,
        symbolsLayout: Int,
        symbolsShiftedLayout: Int,
        multiKeys: Boolean
    ) {
        currentMode = if (multiKeys) KeyboardModes.MULTI_KEY else KeyboardModes.REGULAR
        lettersKeyboard = PortraitLatinQwertyKeyboard(mContext, lettersLayout, multiKeys)
        lettersAccKeyboard = PortraitLatinQwertyKeyboard(mContext, lettersAccLayout, multiKeys)
        symbolsKeyboard = PortraitLatinSymbolsKeyboard(mContext, symbolsLayout)
        symbolsShiftedKeyboard = PortraitLatinSymbolsKeyboard(mContext, symbolsShiftedLayout)
    }

    private val displayHeightInPortrait: Int
        private get() {
            val height = mResources.displayMetrics.heightPixels
            val width = mResources.displayMetrics.widthPixels
            return if (height > width) height else width
        }

    private val displayWidthInPortrait: Int
        private get() {
            val height = mResources.displayMetrics.heightPixels
            val width = mResources.displayMetrics.widthPixels
            return if (height > width) width else height
        }

    companion object {
        protected const val CODES_PER_KEY_9 = 9
        protected const val CODES_PER_KEY_5 = 5
    }

    init {
        mDisplayHeightInPortrait = displayHeightInPortrait
        mDisplayWidthInPortrait = displayWidthInPortrait
    }
}