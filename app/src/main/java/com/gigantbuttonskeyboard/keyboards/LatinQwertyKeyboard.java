/*
 * Copyright (C) 2008-2009 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.gigantbuttonskeyboard.keyboards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gigantbuttonskeyboard.LatinKey;
import com.gigantbuttonskeyboard.LatinKeyboardView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.inputmethodservice.Keyboard;

public class LatinQwertyKeyboard extends Keyboard {
	
	private boolean mMultiKeysPresent = false;
	private List<LayerDrawable> mLayerDrawablesLowerCase = new ArrayList<LayerDrawable>();
	private List<LayerDrawable> mLayerDrawablesUpperCase = new ArrayList<LayerDrawable>();
	protected int mBottomPadding = 0;

    @SuppressLint("NewApi")
	public LatinQwertyKeyboard(Context context, int xmlLayoutResId, boolean multiKeysPresent, int orientation) {
        super(context, xmlLayoutResId, 0);
        mMultiKeysPresent = multiKeysPresent;
        List<Key> keys = this.getKeys();
        
        int rowId = 0;
        int idInRow = 0;
        for(Key key : keys) {
        	LatinKey latinKey = (LatinKey) key;

			if (latinKey.keyType == LatinKey.KEY_TYPES.BIG_SLIDING) {
				LayerDrawable lowerLetterIcon = createLayerDrawableForMultiLowerLettersKey(latinKey, orientation);
				LayerDrawable upperLetterIcon = createLayerDrawableForMultiUpperLettersKey(latinKey, orientation);
				mLayerDrawablesLowerCase.add(lowerLetterIcon); 
				mLayerDrawablesUpperCase.add(upperLetterIcon);
				
				latinKey.icon = lowerLetterIcon;
			} else {
				if(KeysDrawables.specialKeyCodes.contains(latinKey.codes[0])) {
					latinKey.icon = KeysDrawables.createRegularKeyDrawable(latinKey.codes[0], orientation);
				}
			}
			
			latinKey.setRowId(rowId);
			latinKey.setIdInRow(idInRow);
        	++idInRow;
			if(latinKey.edgeFlags == Keyboard.EDGE_RIGHT) {
        		++rowId;
        		idInRow = 0;
        	}
        }
    }
     
    @SuppressLint("NewApi")
	public LatinQwertyKeyboard(Context context, int layoutTemplateResId, 
            CharSequence characters, int columns, int horizontalPadding) {
        super(context, layoutTemplateResId, characters, columns, horizontalPadding);
    }
    
    private LayerDrawable createLayerDrawableForMultiUpperLettersKey(LatinKey key, int orientation) {
    	return KeysDrawables.createMultiKeyDrawable(key, true, orientation);
    }
    
    private LayerDrawable createLayerDrawableForMultiLowerLettersKey(LatinKey key, int orientation) {
    	return KeysDrawables.createMultiKeyDrawable(key, false, orientation);
    }

    public void switchCodesToUpperCase() {
    	switchLetterCase(mLayerDrawablesUpperCase);
    }
     
    public void switchCodesToLowerCase() {
    	switchLetterCase(mLayerDrawablesLowerCase);
    }
    
    @SuppressLint("NewApi")
	public void switchLetterCase(List<LayerDrawable> layerDrawablesLowerCase) {
    	if(mMultiKeysPresent) {
    		List<Key> keys = this.getKeys();
        	for (int i=0;i<keys.size();++i) {
        		LatinKey lKey = (LatinKey)keys.get(i);
        		lKey.icon = layerDrawablesLowerCase.get(i);
        	}
        	LatinKeyboardView.getLatinKeyboardView().invalidateAllKeys();
    	}
    }

	@Override
	public int getHeight() {
		return super.getHeight() + mBottomPadding;
	}
}
