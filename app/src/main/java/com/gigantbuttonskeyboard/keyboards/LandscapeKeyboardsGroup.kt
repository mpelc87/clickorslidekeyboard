package com.gigantbuttonskeyboard.keyboards

import com.gigantbuttonskeyboard.R
import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardModes

class LandscapeKeyboardsGroup(keyboardLayout: KeyboardModes) : KeyboardsGroup() {

    init {
        if (keyboardLayout == KeyboardModes.REGULAR) {
            currentMode = KeyboardModes.REGULAR
            initKeyboardsGroupToRegular()
        } else {
            currentMode = KeyboardModes.MULTI_KEY
            initKeyboardsGroupToMulti()
        }
    }

    fun initKeyboardsGroupToRegular() {
        initWithLayout(
            lettersLayout = R.xml.landscape_regular_qwerty,
            lettersAccLayout = R.xml.landscape_regular_qwerty_acc,
            symbolsLayout = R.xml.landscape_regular_symbols,
            symbolsShiftedLayout = R.xml.landscape_regular_symbols_shift,
            multiKeys = false
        )
    }

    fun initKeyboardsGroupToMulti() {
        initWithLayout(
            lettersLayout = R.xml.landscape_multi_letters,
            lettersAccLayout = R.xml.landscape_multi_letters_acc,
            symbolsLayout = R.xml.landscape_multi_symbols,
            symbolsShiftedLayout = R.xml.landscape_multi_symbols,
            multiKeys = true
        )
    }
}