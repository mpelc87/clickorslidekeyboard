package com.gigantbuttonskeyboard.keyboards

import android.preference.PreferenceManager
import com.gigantbuttonskeyboard.R
import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardModes

class PortraitKeyboardsGroup(keyboardMode: KeyboardModes) : KeyboardsGroup() {
    enum class MultiKeyLayout {
        OLD_QWERTY_LIKE, OLD_3_X_4, QWERTY_LIKE
    }

    init {
        if (keyboardMode == KeyboardModes.REGULAR) {
            currentMode = KeyboardModes.REGULAR
            initKeyboardsGroupToRegular()
        } else {
            currentMode = KeyboardModes.MULTI_KEY
            initKeyboardsGroupToMulti()
        }
    }

    fun initKeyboardsGroupToRegular() {
        currentMode = KeyboardModes.REGULAR
        initWithLayout(
            lettersLayout = R.xml.regular_qwerty,
            lettersAccLayout = R.xml.regular_qwerty_acc,
            symbolsLayout = R.xml.regular_symbols,
            symbolsShiftedLayout = R.xml.regular_symbols_shift,
            multiKeys = false
        )
    }

    fun initKeyboardsGroupToMulti() {
        val sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(mContext)
        val modeFromPreferences = Integer.valueOf(
            sharedPreferences.getString(
                mContext.getString(R.string.prefer_multi_letter_layout),
                MultiKeyLayout.OLD_3_X_4.ordinal.toString()
            )!!
        )
        when(modeFromPreferences) {
            MultiKeyLayout.OLD_QWERTY_LIKE.ordinal ->
                initWithLayout(
                    lettersLayout = R.xml.layout_old_multi_letters,
                    lettersAccLayout = R.xml.layout_old_multi_letters_acc,
                    symbolsLayout = R.xml.layout_old_multi_symbols,
                    symbolsShiftedLayout = R.xml.layout_old_multi_symbols,
                    multiKeys = true
                )
            MultiKeyLayout.QWERTY_LIKE.ordinal ->
                initWithLayout(
                    lettersLayout = R.xml.layout_multi_letters,
                    lettersAccLayout = R.xml.layout_multi_letters_acc,
                    symbolsLayout = R.xml.layout_multi_symbols,
                    symbolsShiftedLayout = R.xml.layout_multi_symbols,
                    multiKeys = true
                )
            else ->
                initWithLayout(
                    lettersLayout = R.xml.layout_3x4_multi_letters,
                    lettersAccLayout = R.xml.layout_3x4_multi_letters_acc,
                    symbolsLayout = R.xml.layout_3x4_multi_symbols,
                    symbolsShiftedLayout = R.xml.layout_3x4_multi_symbols,
                    multiKeys = true
                )

        }
    }

}