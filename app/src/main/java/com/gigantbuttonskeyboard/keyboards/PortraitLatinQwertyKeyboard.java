package com.gigantbuttonskeyboard.keyboards;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.LatinKey;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.preference.PreferenceManager;

public class PortraitLatinQwertyKeyboard extends LatinQwertyKeyboard {
	
	public PortraitLatinQwertyKeyboard(Context context, int xmlLayoutResId, boolean multiKeysPresent) {
		super(context, xmlLayoutResId, multiKeysPresent, Configuration.ORIENTATION_PORTRAIT);
		
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mBottomPadding = sharedPreferences.getInt(context.getString(R.string.prefer_portrait_botton_padding_height), 0);
	}
	
	@SuppressLint("NewApi")  
	@Override
    protected Key createKeyFromXml(Resources res, Row parent, int x, int y, 
            XmlResourceParser parser) {
    	LatinKey latinKey = new LatinKey(res, parent, x, y, parser, Configuration.ORIENTATION_PORTRAIT);
        return latinKey; 
    }
}
