package com.gigantbuttonskeyboard

import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView

class SoftOnKeyboardActionListener(private val softKeyboard: SoftKeyboard) : KeyboardView.OnKeyboardActionListener {

    override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
        with (softKeyboard) {
            wasKeyPressed = true
            if (LatinKeyboardView.getIsKeyPressed()) {
                when (primaryCode) {
                    SoftKeyboard.CODE_ENTER -> {
                        if (composing!!.isNotEmpty()) {
                            commitTyped(currentInputConnection)
                        }
                        handleEnter()
                    }
                    Keyboard.KEYCODE_DELETE -> handleBackspace()
                    Keyboard.KEYCODE_SHIFT -> handleShift()
                    Keyboard.KEYCODE_CANCEL -> handleClose()
                    LatinKeyboardView.KEYCODE_SETTINGS -> startPreferences()
                    LatinKeyboardView.KEYCODE_OPTIONS -> { /* Show a menu or somethin' */ }
                    Keyboard.KEYCODE_MODE_CHANGE -> handleModeChange()
                    KEYCODE_MODE_CHANGE_TO_LETTERS -> handleModeChangeToLetters()
                    KEYCODE_ACCENTS -> handleAccent()
                    SoftKeyboard.CHANGE_KEYBOARDS_LAYOUT, SoftKeyboard.CHANGE_KEYBOARDS_LAYOUT_2 -> {
                        updateCurrentLayouts()
                        handleKeyboardLayoutChange()
                        currentLetterMode = SoftKeyboard.CurrentKeyboardModes.LETTERS
                    }
                    KEYCODE_EMOJI -> setKeyboardViewTypeToEmoji()
                    else -> {
                        if (SoftKeyboardHelper.isWordSeparator(primaryCode)) {
                            if (composing!!.isNotEmpty()) {
                                commitTyped(currentInputConnection)
                            }
                            currentInputConnection.commitText((primaryCode.toChar()).toString(), 1)
                            updateShiftKeyState(currentInputEditorInfo)
                        } else {
                            handleCharacter(primaryCode)
                        }
                    }
                }
            }
        }
    }


    //=================================== Disabled functionalities ============================================

    override fun onPress(primaryCode: Int) {
        // TODO Auto-generated method stub
    }

    override fun onRelease(primaryCode: Int) {
        // TODO Auto-generated method stub
    }

    override fun onText(text: CharSequence) {
        // TODO Auto-generated method stub
    }

    override fun swipeLeft() {
        // TODO Auto-generated method stub
    }

    override fun swipeRight() {
        // TODO Auto-generated method stub
    }

    override fun swipeDown() {
        // TODO Auto-generated method stub
    }

    override fun swipeUp() {
        // TODO Auto-generated method stub
    }

    companion object {
        private const val KEYCODE_MODE_CHANGE_TO_LETTERS = -22
        private const val KEYCODE_ACCENTS = -101
        private const val KEYCODE_EMOJI = -105
    }
}