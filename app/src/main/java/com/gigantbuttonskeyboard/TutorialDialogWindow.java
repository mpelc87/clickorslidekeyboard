package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TutorialDialogWindow extends Activity {
	private AlertDialog alertDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    AlertDialog.Builder builder = initBuilder();
	    alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				dialog.cancel();
                finish();
			}
		});
	    alertDialog.show();
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private AlertDialog.Builder initBuilder() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View tutorialDialogView = getLayoutInflater().inflate(R.layout.tutorial_dialog, null);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
	    	Point size = new Point();
		    getWindowManager().getDefaultDisplay().getSize(size);
		    tutorialDialogView.setMinimumWidth((int)(size.x * 0.9));
	    } else {
	    	tutorialDialogView.setMinimumWidth((int)(getWindowManager().getDefaultDisplay().getWidth() * 0.9));
	    }
		
	    builder.setView(tutorialDialogView);

	    Button okButton = (Button) tutorialDialogView.findViewById(R.id.tutorial_ok_button);
	    okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
	    
	    return builder;
	}
	
}