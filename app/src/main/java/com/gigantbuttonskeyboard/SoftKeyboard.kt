package com.gigantbuttonskeyboard

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Point
import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.preference.PreferenceManager
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.CompletionInfo
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.ExtractedTextRequest
import android.view.inputmethod.InputConnection
import android.widget.Toast
import com.gigantbuttonskeyboard.emoji.Emoji
import com.gigantbuttonskeyboard.keyboards.KeyboardsAll
import com.gigantbuttonskeyboard.keyboards.LatinQwertyKeyboard
import com.gigantbuttonskeyboard.keyboards.PortraitKeyboardsGroup
import com.gigantbuttonskeyboard.preferences.ImePreferences
import com.gigantbuttonskeyboard.suggestions.SuggestionsController
import com.gigantbuttonskeyboard.suggestions.SuggestionsHelper
import java.util.Locale

class SoftKeyboard : InputMethodService() {
    private lateinit var suggestionsController: SuggestionsController
    internal var inputView: LatinKeyboardView? = null
    private var emoji: Emoji? = null
    internal var candidateView: CandidateView? = null
    internal lateinit var onKeyboardActionListener: SoftOnKeyboardActionListener
    internal val composing: StringBuilder? = StringBuilder()
    private var completions: Array<CompletionInfo>? = null
    private var completionOn = false
    private var suggestions: ArrayList<String>? = null
    var suggestionsOn = false
        private set
    private var suggestionsAccentsOn = false
    private var actionOnEnter = false
    private var lastDisplayWidth = 0
    private var capsLock = false
    private var lastShiftTime: Long = 0
    var softKeyboardContext: Context? = null
        private set
    private var keyboards: KeyboardsAll? = null
    var deviceLanguage: String? = null
        private set
    var wasKeyPressed = false
    private var displaySize: Point? = null
    var statusBarHeight = 0
        private set
    private var currentPortraitLayout: KeyboardModes? = KeyboardModes.MULTI_KEY
    private var currentLandscapeLayout: KeyboardModes? = KeyboardModes.REGULAR
    var preferencesListener: OnSharedPreferenceChangeListener? = null
    private var suggestionsDepth = 1
    var editTextVariation = 0

    enum class KeyboardModes {
        REGULAR, MULTI_KEY
    }

    enum class CurrentKeyboardModes {
        LETTERS, LETTERS_ACC, SYMBOLS, SYMBOLS_SHIFTED
    }

    enum class KeyboardViewType {
        KEYBOARD, EMOJI
    }

    internal var keepLetterAccent = false
    internal var currentLetterMode = CurrentKeyboardModes.LETTERS
    private var currentKeyboardViewType = KeyboardViewType.KEYBOARD
    private var sharedPreferences: SharedPreferences? = null

    enum class KeyboardLanguages {
        PL, EN, DE
    }

    internal var currentKeyboardLang = KeyboardLanguages.EN
    val composingLength: Int
        get() = composing!!.length

    private fun loadStatusBarHeightFromConfiguration(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun loadDisplaySizeFromConfiguration(): Point {
        val wm = getSystemService(WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    private val candidatesViewHeight: Int
        get() = if (candidateView != null) {
            candidateView!!.height
        } else 0
    private val displayHeight: Int
        get() = if (displaySize != null) {
            displaySize!!.y
        } else 0
    val inputInFullScreenHeight: Int
        get() = displayHeight - LatinKeyboardView.getLatinKeyboardView().height - candidatesViewHeight - statusBarHeight

    /**
     * onCreate is called only once on process start
     */
    override fun onCreate() {
        super.onCreate()
        onKeyboardActionListener = SoftOnKeyboardActionListener(this)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        deviceLanguage = Locale.getDefault().language
        softKeyboard = this
        softKeyboardContext = this.baseContext
        suggestionsController = SuggestionsController(baseContext)
        checkAppVersion()
        checkLicenseAcceptance()
        setupKeyboardLanguage()
        SuggestionsHelper.setUpDictionaries(softKeyboardContext)
        createPreferencesListener()
    }

    private fun checkAppVersion() {
        val versionFromInternalStorage =
            sharedPreferences!!.getInt(getString(R.string.app_current_version_code_key), 0)
        val codeVersionRequireCleanUp =
            resources.getInteger(R.integer.app_version_code_require_clean_up_value)
        if (versionFromInternalStorage <= codeVersionRequireCleanUp) {
            val wasLicenceAccepted = sharedPreferences!!.getBoolean(
                resources.getString(R.string.prefer_licence_acceptance),
                false
            )
            if (versionFromInternalStorage != 0 || wasLicenceAccepted) { //TODO: remove wasLicenceAccepted check for later version
                showToast(getString(R.string.app_version_incompatible_message), Toast.LENGTH_LONG)
                SuggestionsHelper.cleanInternalStorage(this)
            }
        }
        val codeVersionToKeepLayout = resources.getInteger(R.integer.app_version_code_keep_layout)
        if (versionFromInternalStorage <= codeVersionToKeepLayout && versionFromInternalStorage > 0) {
            val modeFromPreferences = sharedPreferences!!.getString(
                softKeyboardContext!!.getString(R.string.prefer_multi_letter_layout),
                PortraitKeyboardsGroup.MultiKeyLayout.QWERTY_LIKE.ordinal.toString()
            )!!
                .toInt()
            val editor = sharedPreferences!!.edit()
            editor.putString(
                softKeyboardContext!!.getString(R.string.prefer_multi_letter_layout),
                modeFromPreferences.toString()
            )
            editor.apply()
        }
        val editor = sharedPreferences!!.edit()
        var currentVersionCode = 0
        try {
            currentVersionCode = packageManager.getPackageInfo(packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
        }
        editor.putInt(getString(R.string.app_current_version_code_key), currentVersionCode)
        editor.apply()
    }

    private fun checkLicenseAcceptance() {
        if (!sharedPreferences!!.getBoolean(getString(R.string.prefer_licence_acceptance), false)) {
            startAcceptLicenceDialogWindow()
        }
    }

    private fun startAcceptLicenceDialogWindow() {
        val intent = Intent(this@SoftKeyboard, LicenceDialogWindow::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    fun setupKeyboardLanguage() {
        val userPreferenceKeyboardLanguage =
            sharedPreferences!!.getString(getString(R.string.prefer_keyboard_user_language), "auto")
        val editor = sharedPreferences!!.edit()
        if (userPreferenceKeyboardLanguage == "auto") {
            editor.putString(
                getString(R.string.keyboard_current_language), LanguagesHelper.getLangCode(
                    deviceLanguage
                )
            )
        } else {
            editor.putString(
                getString(R.string.keyboard_current_language),
                userPreferenceKeyboardLanguage
            )
            updateLocale(userPreferenceKeyboardLanguage)
        }
        editor.apply()
    }

    fun updateLocale(localeIndex: String?) {
        val locale = Locale(localeIndex)
        val config = Configuration()
        config.locale = locale
        softKeyboardContext!!.resources.updateConfiguration(config, this.resources.displayMetrics)
    }

    /**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     * NOTE: Fires on process start-up and on screen orientation change
     */
    override fun onCreateInputView(): View {
        updateCurrentKeyboardLanguageSharedPrefernce()
        displaySize = loadDisplaySizeFromConfiguration()
        statusBarHeight = loadStatusBarHeightFromConfiguration()
        setupKeyboardView()
        makeKeyboardsIfScreenOrientationChange()
        LatinKeyboardView.setSoftKeyboard(this)
        currentLetterMode = CurrentKeyboardModes.LETTERS
        return inputView!!
    }

    private fun setupKeyboardView() {
        inputView = layoutInflater.inflate(R.layout.input, null) as LatinKeyboardView
        inputView!!.setOnKeyboardActionListener(onKeyboardActionListener)
        inputView!!.isPreviewEnabled = false
        currentKeyboardViewType = KeyboardViewType.KEYBOARD
    }

    fun setKeyboardViewTypeToKeyboard() {
        setCandidatesViewShown(true)
        currentKeyboardViewType = KeyboardViewType.KEYBOARD
        setInputView(inputView)
    }

    internal fun setKeyboardViewTypeToEmoji() {
        setCandidatesViewShown(false)
        if (emoji == null) {
            emoji = Emoji(this)
        }
        emoji!!.updateEmojiGridHeight(loadDisplaySizeFromConfiguration().y / 3)
        currentKeyboardViewType = KeyboardViewType.EMOJI
        val emojiView = emoji!!.emojiView
        if (emojiView.parent != null) {
            (emojiView.parent as ViewGroup).removeView(emojiView)
        }
        setInputView(emojiView)
    }

    private fun makeKeyboardsIfScreenOrientationChange() {
        if (keyboards != null) {
            val displayWidth = maxWidth
            if (displayWidth == lastDisplayWidth) {
                return
            }
            lastDisplayWidth = displayWidth
            keyboards!!.recreateKeyboardsGroupsOnOrientation(
                resources.configuration.orientation,
                currentPortraitLayout,
                currentLandscapeLayout
            )
        } else {
            keyboards = KeyboardsAll(currentPortraitLayout, currentLandscapeLayout)
        }
    }

    /**
     * Helper function to generate the various keyboard layouts used by the
     * input method.  Takes care of regenerating the layouts if the width
     * of the input method changes.
     */
    internal fun makeKeyboardsIfNull() {
        if (keyboards == null) {
            keyboards = KeyboardsAll(currentPortraitLayout, currentLandscapeLayout)
        }
    }

    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     * NOTE: Fires on process start-up and every time when the keyboard pops up
     */
    override fun onStartInputView(attribute: EditorInfo, restarting: Boolean) {
        super.onStartInputView(attribute, restarting)
        setKeyboardViewTypeToKeyboard()
        checkSuggestionsSettings()
        keepLetterAccent = keepLetterAccentFromPreference

        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
//        LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5);
        suggestionsOn = false
        completionOn = false
        actionOnEnter = false
        completions = null
        suggestions = null
        val keyboard: Keyboard
        val orientation = resources.configuration.orientation
        when (attribute.inputType and EditorInfo.TYPE_MASK_CLASS) {
            EditorInfo.TYPE_CLASS_NUMBER, EditorInfo.TYPE_CLASS_DATETIME, EditorInfo.TYPE_CLASS_PHONE -> keyboard =
                keyboards!!.getSymbolsKeyboardFromOrientation(orientation)
            EditorInfo.TYPE_CLASS_TEXT -> {
                if (currentLetterMode == CurrentKeyboardModes.LETTERS_ACC) {
                    keyboard = keyboards!!.getLettersAccKeyboardFromOrientation(orientation)
                    LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_9)
                } else {
                    keyboard = keyboards!!.getLettersKeyboardFromOrientation(orientation)
                    LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
                }
                configureForText(attribute)
                updateShiftKeyState(attribute)
            }
            else -> keyboard = keyboards!!.getLettersKeyboardFromOrientation(orientation)
        }
        if (inputView != null) {
            inputView!!.keyboard = keyboard
            inputView!!.closing()
        }
        setCandidatesViewShown(suggestionsOn)
        composing!!.setLength(0)
        updateCandidates()
        //note for TYPE_TEXT_VARIATION_WEB_EDIT_TEXT
        //onStartInputView executes always when composing is active and we change cursor position
        //from one word to the other
        returnToComposingIfAppropriate()

//        ((View)mInputView.getParent()).setBackgroundColor(Color.BLACK);
    }

    /**
     * NOTE: Method cannot be called inside
     * onCreateInputView() because of framework limitation
     */
    private fun configureForText(attribute: EditorInfo) {
        suggestionsOn = sharedPreferences!!.getBoolean(
            getString(R.string.prefer_suggestions_functionality),
            true
        )
        if (suggestionsOn) {
            val variation = attribute.inputType and EditorInfo.TYPE_MASK_VARIATION
            editTextVariation = variation
            if (variation == EditorInfo.TYPE_TEXT_VARIATION_PASSWORD || variation == EditorInfo.TYPE_TEXT_VARIATION_WEB_PASSWORD || variation == EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD || variation == EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS || variation == EditorInfo.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS) {
                suggestionsOn = false
            }
            if (attribute.inputType and EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE != 0) {
                suggestionsOn = false
                completionOn = isFullscreenMode
            }
            if (attribute.inputType and EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS != 0) {
                suggestionsOn = false
            }
        }
        if (attribute.inputType and EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE == 0) {
            actionOnEnter = true
        }
    }

    private fun checkSuggestionsSettings() {
        if (sharedPreferences!!.getBoolean(
                getString(R.string.prefer_suggestions_functionality),
                true
            )
        ) {
            if (suggestionsController.suggestionsMap == null) {
                suggestionsController.generateSuggestions()
            }
        }
        if (!sharedPreferences!!.getBoolean(
                getString(R.string.prefer_suggestions_functionality),
                true
            )
        ) {
            if (suggestionsController.suggestionsMap != null) {
                suggestionsController.suggestionsMap = null
                System.gc()
            } else {
                stopGenerationIfInProgress()
            }
        }
        suggestionsDepth = Integer.valueOf(
            sharedPreferences!!.getString(
                getString(R.string.prefer_suggestions_min_depth),
                "1"
            )!!
        )
        suggestionsAccentsOn = if (currentKeyboardLang == KeyboardLanguages.PL) {
            sharedPreferences!!.getBoolean(getString(R.string.prefer_suggestions_accents), true)
        } else {
            true
        }
    }

    private val keepLetterAccentFromPreference: Boolean
        get() = if (currentKeyboardLang == KeyboardLanguages.PL) {
            sharedPreferences!!.getBoolean(getString(R.string.prefer_keep_accents), true)
        } else false

    private fun updateCurrentKeyboardLanguageSharedPrefernce() {
        val userPreferenceKeyboardLanguage =
            sharedPreferences!!.getString(getString(R.string.prefer_keyboard_user_language), "auto")
        var currentKeyboardLanguage = sharedPreferences!!.getString(
            getString(R.string.keyboard_current_language),
            getString(R.string.default_keyboard_language)
        )
        if (userPreferenceKeyboardLanguage == "auto") {
            if (currentKeyboardLanguage != deviceLanguage) {
                updateKeyboardLanguage(LanguagesHelper.getLangCode(deviceLanguage))
            }
        } else { //language overridden manually by preference
            if (userPreferenceKeyboardLanguage != currentKeyboardLanguage) {
                updateKeyboardLanguage(userPreferenceKeyboardLanguage)
            }
        }
        currentKeyboardLanguage = sharedPreferences!!.getString(
            getString(R.string.keyboard_current_language),
            getString(R.string.default_keyboard_language)
        )
        updateLocale(currentKeyboardLanguage)
        currentKeyboardLang = LanguagesHelper.getLangEnum(currentKeyboardLanguage)
    }

    private fun updateKeyboardLanguage(language: String?) {
        val editor = sharedPreferences!!.edit()
        editor.putString(getString(R.string.keyboard_current_language), language)
        editor.apply()
    }

    fun destroyKeyboards() {
        if (keyboards != null) {
            keyboards = null
        }
    }

    internal fun stopGenerationIfInProgress() {
        if (suggestionsController.suggestionsLoadingAsyncTask != null) {
            suggestionsController.generationStop = true
        }
    }

    private fun createPreferencesListener() {
        preferencesListener = SoftOnSharedPreferenceChangeListener(this)
        sharedPreferences!!.registerOnSharedPreferenceChangeListener(preferencesListener)
    }

    internal fun handleModeChange() {
        LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
        val newCurrent =
            keyboards!!.getSymbolsKeyboardFromOrientation(resources.configuration.orientation)
        newCurrent.isShifted = false
        inputView!!.keyboard = newCurrent
    }

    internal fun handleModeChangeToLetters() {
        if (currentLetterMode == CurrentKeyboardModes.LETTERS) {
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
            inputView!!.keyboard =
                keyboards!!.getLettersKeyboardFromOrientation(resources.configuration.orientation)
            currentLetterMode = CurrentKeyboardModes.LETTERS
        } else {
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_9)
            inputView!!.keyboard =
                keyboards!!.getLettersAccKeyboardFromOrientation(resources.configuration.orientation)
            currentLetterMode = CurrentKeyboardModes.LETTERS_ACC
        }
    }

    internal fun handleAccent() {
        val isShifted = inputView!!.isShifted
        if (currentLetterMode == CurrentKeyboardModes.LETTERS) {
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_9)
            inputView!!.keyboard =
                keyboards!!.getLettersAccKeyboardFromOrientation(resources.configuration.orientation)
            currentLetterMode = CurrentKeyboardModes.LETTERS_ACC
        } else {
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
            inputView!!.keyboard =
                keyboards!!.getLettersKeyboardFromOrientation(resources.configuration.orientation)
            currentLetterMode = CurrentKeyboardModes.LETTERS
        }
        changeKeyboardOnShiftState(isShifted)
    }

    internal fun destroySuggestions() {
        stopGenerationIfInProgress()
        suggestionsController.suggestionsMap = null
        System.gc()
    }

    internal fun updateCurrentLayouts() {
        if (keyboards != null) {
            val portraitKeyboardsGroup = keyboards!!.portraitKeyboardsGroup
            val landscapeKeyboardsGroup = keyboards!!.landscapeKeyboardsGroup
            if (portraitKeyboardsGroup != null) {
                currentPortraitLayout = portraitKeyboardsGroup.currentMode
            }
            if (landscapeKeyboardsGroup != null) {
                currentLandscapeLayout = landscapeKeyboardsGroup.currentMode
            }
        }
    }

    /**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    override fun onFinishInput() {
        super.onFinishInput()
        // We only hide the candidates window when finishing input on
        // a particular editor, to avoid popping the underlying application
        // up and down if the user is entering text into the bottom of
        // its window.
        setCandidatesViewShown(false)
        if (inputView != null) {
            inputView!!.closing()
        }
    }

    override fun onFinishInputView(finishingInput: Boolean) {
        super.onFinishInputView(finishingInput)
        suggestionsOn = false
    }

    /**
     * Deal with the editor reporting movement of its cursor.
     */
    @Synchronized
    override fun onUpdateSelection(
        oldSelStart: Int, oldSelEnd: Int,
        newSelStart: Int, newSelEnd: Int,
        candidatesStart: Int, candidatesEnd: Int
    ) {
        if (wasKeyPressed || LatinKeyboardView.getIsKeyPressed()) { //if this function was/is caused by key press we should do nothing
            wasKeyPressed = false
            return
        }
        if (composing!!.isNotEmpty() && (newSelStart != candidatesEnd || newSelEnd != candidatesEnd) && newSelStart != oldSelStart) {
            composing.setLength(0)
            updateCandidates()
            val ic = currentInputConnection
            ic?.finishComposingText()
        }
        if (newSelStart == newSelEnd) {
            returnToComposingIfAppropriate()
        }
    }

    @Synchronized
    fun returnToComposingIfAppropriate() {
        if (suggestionsOn && composing!!.isEmpty() && editTextVariation != EditorInfo.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT) { //WORKAROUND: for web edit text strange things happens and this functionality needs to be disabled
            val inputConnection = currentInputConnection
            if (inputConnection != null) {
                val extractedText = inputConnection.getExtractedText(ExtractedTextRequest(), 0)
                if (extractedText != null) {
                    val cursorPosition = extractedText.selectionStart
                    if (cursorPosition > 0 && extractedText.text.length > 0) {
                        val chars = extractedText.text
                        val currentChar = chars[cursorPosition - 1]
                        if (Character.isLetter(currentChar)) {
                            val newComposing =
                                getLettersBeforePosition(chars.toString(), cursorPosition - 1)
                            val newComposingLen = newComposing.length
                            if (newComposingLen <= MAX_COMPOSING_LEN) {
                                composing.append(newComposing)
                                updateCandidates()
                                inputConnection.setComposingRegion(
                                    cursorPosition - newComposingLen,
                                    cursorPosition
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    fun checkIfComposingWillBeLessThenMax(): Boolean {
        if (suggestionsOn) {
            val inputConnection = currentInputConnection
            val extractedText = inputConnection.getExtractedText(ExtractedTextRequest(), 0)
            if (extractedText != null) {
                val cursorPosition = extractedText.selectionStart
                if (cursorPosition > 0 && extractedText.text.length > 0) {
                    val chars = extractedText.text
                    val currentChar = chars[cursorPosition - 1]
                    if (Character.isLetter(currentChar)) {
                        val newComposing =
                            getLettersBeforePosition(chars.toString(), cursorPosition - 1)
                        val newComposingLen = newComposing.length
                        if (newComposingLen >= MAX_COMPOSING_LEN) {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }

    private fun getLettersBeforePosition(input: String, position: Int): String {
        val output = StringBuilder("")
        if (position < input.length) {
            for (i in position downTo 0) {
                val currentChar = input[i]
                if (Character.isLetter(currentChar)) {
                    output.append(currentChar)
                } else {
                    break
                }
            }
        }
        return StringBuilder(output).reverse().toString()
    }

    fun onEmojiPress(emoji: CharSequence?) {
        if (composing!!.isNotEmpty()) {
            commitTyped(currentInputConnection)
        }
        currentInputConnection.commitText(emoji, 1)
        updateShiftKeyState(currentInputEditorInfo)
    }

    fun onEmojiKeyPress(keyCode: Int) {
        if (keyCode == CODE_ENTER) {
            if (composing!!.isNotEmpty()) {
                commitTyped(currentInputConnection)
            }
            handleEnter()
        } else if (keyCode == Keyboard.KEYCODE_DELETE) {
            handleBackspace()
        } else if (keyCode == LatinKeyboardView.KEYCODE_SETTINGS) {
            startPreferences()
        } else {
            if (composing!!.isNotEmpty()) {
                commitTyped(currentInputConnection)
            }
            currentInputConnection.commitText((keyCode.toChar()).toString(), 1)
            updateShiftKeyState(currentInputEditorInfo)
        }
    }

    fun handleKeyboardLayoutChange() {
        val orientation = resources.configuration.orientation
        val wasShifted = inputView!!.isShifted
        val currentLayout = keyboards!!.getKeyboardsGroupFromOrientation(orientation).currentMode
        if (currentLayout == KeyboardModes.REGULAR) {
            keyboards!!.reinitKeyboardsGroupToMulti(orientation)
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
        } else {
            keyboards!!.reinitKeyboardsGroupToRegular(orientation)
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
        }
        inputView!!.keyboard =
            keyboards!!.getKeyboardFromModeAndOrientation(
                CurrentKeyboardModes.LETTERS,
                orientation
            )
        changeKeyboardOnShiftState(wasShifted)
        updateCurrentLayouts()
    }

    fun startSettingsActivity() {
        val intent = Intent(this@SoftKeyboard, ImePreferences::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        startActivity(intent)
    }

    internal fun startPreferences() {
        val intent = Intent(this@SoftKeyboard, ImePreferences::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    internal fun handleEnter() {
        if (actionOnEnter) {
            keyDownUp(KeyEvent.KEYCODE_ENTER)
        } else {
            currentInputConnection.commitText((CODE_ENTER.toChar()).toString(), 1)
        }
    }

    internal fun handleBackspace() {
        val length = composing!!.length
        if (length > 1) {
            composing.delete(length - 1, length)
            currentInputConnection.setComposingText(composing, 1)
        } else if (length > 0) {
            composing.setLength(0)
            currentInputConnection.commitText("", 0)
        } else {
            keyDownUp(KeyEvent.KEYCODE_DEL)
        }
        updateShiftKeyState(currentInputEditorInfo)
    }

    fun handleFastBackspace(amountOfCharsToRemove: Int) {
        val inputConnection = currentInputConnection
        val extractedText = inputConnection.getExtractedText(ExtractedTextRequest(), 0)
        if (extractedText != null) {
            val cursorPosition = extractedText.selectionStart
            if (cursorPosition > 0) {
                if (composing != null && composing.isNotEmpty()) {
                    inputConnection.commitText(composing, 1)
                    composing.setLength(0)
                }
                val realAmountToRemove =
                    if (amountOfCharsToRemove > cursorPosition) cursorPosition else amountOfCharsToRemove
                inputConnection.deleteSurroundingText(realAmountToRemove, 0)
            }
        }
    }

    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private fun keyDownUp(keyEventCode: Int) {
        currentInputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode))
        currentInputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, keyEventCode))
    }

    internal fun handleShift() {
        if (inputView != null) {
            val orientation = resources.configuration.orientation
            val currentKeyboard = inputView!!.keyboard
            if (currentKeyboard === keyboards!!.getLettersKeyboardFromOrientation(orientation) ||
                currentKeyboard === keyboards!!.getLettersAccKeyboardFromOrientation(orientation)
            ) {
                // Alphabet keyboard
                if (capsLock) { //Fix for capslock problems
                    capsLock = false
                } else {
                    checkToggleCapsLock()
                }
                val isShifted = capsLock || !inputView!!.isShifted
                changeKeyboardOnShiftState(isShifted)
                inputView!!.isShifted = isShifted
            } else if (currentKeyboard === keyboards!!.getSymbolsKeyboardFromOrientation(
                    orientation
                )
            ) {
                currentKeyboard.isShifted = true
                inputView!!.keyboard =
                    keyboards!!.getSymbolsShiftedKeyboardFromOrientation(orientation)
                keyboards!!.getSymbolsShiftedKeyboardFromOrientation(orientation).isShifted = true
            } else if (currentKeyboard === keyboards!!.getSymbolsShiftedKeyboardFromOrientation(
                    orientation
                )
            ) {
                currentKeyboard.isShifted = false
                inputView!!.keyboard = keyboards!!.getSymbolsKeyboardFromOrientation(orientation)
                keyboards!!.getSymbolsKeyboardFromOrientation(orientation).isShifted = false
            }
        }
    }

    private fun checkToggleCapsLock() {
        val now = System.currentTimeMillis()
        if (lastShiftTime + 800 > now) {
            capsLock = !capsLock
            lastShiftTime = 0
        } else {
            lastShiftTime = now
        }
    }

    /**
     * Helper to update the shift state of our keyboard based on the initial
     * editor state.
     */
    internal fun updateShiftKeyState(attr: EditorInfo?) {
        val orientation = resources.configuration.orientation
        if (attr != null && inputView != null && (inputView!!.keyboard === keyboards!!.getLettersKeyboardFromOrientation(
                orientation
            )
                || inputView!!.keyboard === keyboards!!.getLettersAccKeyboardFromOrientation(
                orientation
            ))
        ) {
            val caps = currentInputConnection.getCursorCapsMode(attr.inputType)
            val isShifted = capsLock || caps != 0
            changeKeyboardOnShiftState(isShifted)
        }
    }

    private fun changeKeyboardOnShiftState(isShifted: Boolean) {
        if (inputView!!.isShifted != isShifted) {
            if (isShifted) {
                (inputView!!.keyboard as LatinQwertyKeyboard).switchCodesToUpperCase()
            } else {
                (inputView!!.keyboard as LatinQwertyKeyboard).switchCodesToLowerCase()
            }
            inputView!!.isShifted = isShifted
        }
    }

    internal fun handleCharacter(inputPrimaryCode: Int) {
        var primaryCode = inputPrimaryCode
        if (isInputViewShown) {
            if (inputView!!.isShifted) {
                primaryCode = Character.toUpperCase(primaryCode)
            }
        }
        val characterString = (primaryCode.toChar()).toString()
        if (suggestionsOn) {
            if (composing!!.isEmpty() && !Character.isLetter(primaryCode)) {
                currentInputConnection.commitText(characterString, 1)
            } else {
                returnToComposingIfAppropriate()
                val shouldContinueComposing = checkIfComposingWillBeLessThenMax()
                if (shouldContinueComposing) {
                    composing.append(primaryCode.toChar())
                    currentInputConnection.setComposingText(composing, 1)
                    updateShiftKeyState(currentInputEditorInfo)
                    updateCandidates()
                } else {
                    if (composing.isNotEmpty()) {
                        composing.append(primaryCode.toChar())
                        commitTyped(currentInputConnection)
                    } else {
                        currentInputConnection.commitText(characterString, 1)
                    }
                }
            }
        } else {
            //to deal with race condition which sometime incorrectly clears candidates in onUpdateSelection
            currentInputConnection.commitText(characterString, 1)
        }
        updateShiftKeyState(currentInputEditorInfo)

        if (!keepLetterAccent && inputView!!.keyboard === keyboards!!.getLettersAccKeyboardFromOrientation(
                resources.configuration.orientation
            )
        ) {
            inputView!!.keyboard =
                keyboards!!.getLettersKeyboardFromOrientation(resources.configuration.orientation)
            updateShiftKeyState(currentInputEditorInfo)
            LatinKeyboardView.setDirectionsPerKey(CODES_PER_KEY_5)
            currentLetterMode = CurrentKeyboardModes.LETTERS
        }
    }

    internal fun handleClose() {
        commitTyped(currentInputConnection)
        inputView!!.closing()
    }

    /**
     * Helper function to commit any text being composed in to the editor.
     */
    internal fun commitTyped(inputConnection: InputConnection) {
        if (composing!!.isNotEmpty()) {
            inputConnection.commitText(composing, 1)
            composing.setLength(0)
            updateCandidates()
        }
    }

    /**
     * Called by the framework when your view for showing candidates needs to
     * be generated, like [.onCreateInputView].
     */
    override fun onCreateCandidatesView() =
        CandidateView(this).also {
            it.setService(this)
            candidateView = it
        }

    override fun onComputeInsets(outInsets: Insets) {
        super.onComputeInsets(outInsets)
        if (!isFullscreenMode) {
            outInsets.contentTopInsets = outInsets.visibleTopInsets
        }
    }

    /**
     * Update the list of available candidates from the current composing
     * text.  This will need to be filled in by however you are determining
     * candidates.
     */
    fun updateCandidates() {
        if (!completionOn && suggestionsOn) {
            if (composing!!.isNotEmpty() && composing.length <= MAX_COMPOSING_LEN) {
                //call suggestion generator  here
                if (suggestionsController.suggestionsMap != null) {
                    val wordPart = composing.toString()
                    suggestions = suggestionsController.suggestionsMap!!.findSuggestions(
                        wordPart,
                        suggestionsLimit,
                        suggestionsDepth,
                        suggestionsAccentsOn,
                        capsLock,
                        currentKeyboardLang
                    )
                    if (suggestions == null) {
                        suggestions = ArrayList()
                        if (suggestionsController.suggestionsReady) {
                            suggestions!!.add(getString(R.string.add_new_word_message))
                        } else {
                            suggestions!!.add(getString(R.string.preparing_suggestions_message))
                        }
                        setSuggestions(suggestions, true)
                    } else if (suggestions!!.size < suggestionsLimit) {
                        if (!suggestionsContainComposing()) {
                            suggestions!!.add(getString(R.string.add_new_word_message))
                        }
                    }
                } else {
                    suggestions = ArrayList()
                    suggestions!!.add(getString(R.string.preparing_suggestions_message))
                }
                setSuggestions(suggestions, true)
            } else {
                setSuggestions(null, false)
            }
        }
    }

    private fun suggestionsContainComposing(): Boolean {
        return if (suggestionsAccentsOn) {
            suggestions!!.contains(composing.toString())
        } else suggestions!!.contains(SuggestionsHelper.stripAccentFromString(composing.toString()))
    }

    fun setSuggestions(suggestions: List<String>?, typedWordValid: Boolean) {
        if (candidateView != null) {
            candidateView!!.setSuggestions(suggestions, typedWordValid)
            if (suggestions != null && !suggestions.isEmpty()) {
                setCandidatesViewShown(suggestionsOn)
            } else if (isFullscreenMode) {
                setCandidatesViewShown(suggestionsOn)
            }
        }
    }

    fun pickSuggestionManually(index: Int) {
        if (completionOn && completions != null && index >= 0 && index < completions!!.size) {
            val ci = completions!![index]
            currentInputConnection.commitCompletion(ci)
            if (candidateView != null) {
                candidateView!!.clear()
            }
            updateShiftKeyState(currentInputEditorInfo)
        } else if (composing!!.isNotEmpty()) {
            val suggestion = suggestions!![index]
            if (suggestion.contains(getString(R.string.add_new_word_message))) {
                showSuggestionAddDialog(composing.toString())
                return
            } else if (suggestion.contains(getString(R.string.preparing_suggestions_message))) {
                return
            }
            commitSuggestion(currentInputConnection, suggestion)
        }
    }

    private fun commitSuggestion(inputConnection: InputConnection, suggestion: String) {
        if (composing!!.isNotEmpty()) {
            inputConnection.commitText("$suggestion ", 1)
            composing.setLength(0)
            updateCandidates()
        }
    }

    /**
     * This tells us about completions that the editor has determined based
     * on the current text in it.  We want to use this in fullscreen mode
     * to show the completions ourself, since the editor can not be seen
     * in that situation.
     */
    override fun onDisplayCompletions(completions: Array<CompletionInfo>?) {
        if (completionOn) {
            this.completions = completions
            if (completions == null) {
                setSuggestions(null, false)
                return
            }
            val stringList = ArrayList<String>()
            for (i in completions.indices) {
                val ci = completions[i]
                stringList.add(ci.text.toString())
            }
            setSuggestions(stringList, true)
        }
    }

    private fun showSuggestionAddDialog(word: String?) {
        composing!!.setLength(0) //FIXME: Workaround for duplicating words while dialog in Android > 5.0
        val intent = Intent(this@SoftKeyboard, AddSuggestionDialogWindow::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("suggestion", word)
        startActivity(intent)
    }

    fun addNewWordToSuggestions(word: String) {
        if (suggestionsController.suggestionsMap != null) {
            if (!suggestionsController.suggestionsMap!!.isWordInSuggestions(word)) {
                suggestionsController.suggestionsMap!!.addWordToSuggestions(word)
                SuggestionsHelper.addNewWordToDictionary(softKeyboardContext, word)
                commitSuggestion(currentInputConnection, word)
                showToast(getString(R.string.suggestion_added_message), Toast.LENGTH_SHORT)
            } else {
                showToast(
                    getString(R.string.suggestion_already_in_dictionary_message),
                    Toast.LENGTH_SHORT
                )
            }
        }
    }

    fun showSuggestionRemoveDialog(suggestionIndex: Int) {
        if (!suggestionsController.suggestionsReady) {
            showToast(
                "Wait for suggestions to be generated before removing words",
                Toast.LENGTH_SHORT
            )
            return
        }
        if (composing!!.isNotEmpty()) {
            val suggestion = suggestions!![suggestionIndex]
            if (!(suggestion.contains(getString(R.string.add_new_word_message)) || suggestion.contains(
                    getString(R.string.preparing_suggestions_message)
                ))
            ) {
                composing.setLength(0) //FIXME: Workaround for duplicating words while dialog in Android > 5.0
                val intent = Intent(this@SoftKeyboard, RemoveSuggestionDialogWindow::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("suggestion", suggestion)
                startActivity(intent)
            }
        }
    }

    fun removeSuggestion(suggestion: String?) {
        if (suggestionsController.suggestionsMap != null) {
            if (suggestionsController.suggestionsMap!!.isWordInSuggestions(suggestion)) {
                suggestionsController.suggestionsMap!!.removeWordFromSuggestions(
                    suggestion,
                    currentKeyboardLang
                )
                SuggestionsHelper.removeWordFromDictionaryInAsyncTask(
                    softKeyboardContext,
                    suggestion,
                    currentKeyboardLang
                )
                showToast(getString(R.string.suggestion_removed_message), Toast.LENGTH_SHORT)
                updateCandidates()
            } else {
                showToast(
                    getString(R.string.suggestion_not_exist_in_dictionary),
                    Toast.LENGTH_SHORT
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (preferencesListener != null) {
            sharedPreferences!!.unregisterOnSharedPreferenceChangeListener(preferencesListener)
        }
    }

    /* Additional helper methods */

    private fun showToast(message: String, duration: Int) {
        val context = baseContext
        val text: CharSequence = message
        val toast = Toast.makeText(context, text, duration)
        toast.show()
    }

    companion object {
        var softKeyboard: SoftKeyboard? = null
            private set
        private const val CODES_PER_KEY_9 = 9
        private const val CODES_PER_KEY_5 = 5
        const val CODE_ENTER = 10
        const val CHANGE_KEYBOARDS_LAYOUT = -102
        const val CHANGE_KEYBOARDS_LAYOUT_2 = -104
        const val KEYCODE_SPACE = 32
        const val KEYCODE_SETTINGS = -103
        var MAX_COMPOSING_LEN = 50

        // private var mGenerationStop = false
        internal const val DEFAULT_SUGGESTIONS_LIMIT = 10
        internal var suggestionsLimit = DEFAULT_SUGGESTIONS_LIMIT
        // fun setGenerationStop(generationStop: Boolean) {
        //     mGenerationStop = generationStop
        // }
        //
        // @JvmStatic
        // fun shouldStopGeneration(): Boolean {
        //     return mGenerationStop
        // }
    }
}