/*
 * Copyright (C) 2008-2009 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.Keyboard.Key;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

public class LatinKeyboardView extends KeyboardView { 
 
	static final int KEYCODE_OPTIONS = -100;
	static final int KEYCODE_SETTINGS = -103;
	static int mCodesPerKey = 5;
	static LatinKey mPressedKey = null;
	private Context mContext;
	public float mStartingX;
	public float mStartingY;
	private static boolean mIsKeyPressed = false;
	static SoftKeyboard mSoftKeyboard;
	private int mCurrentKeyCodeIndex = 0;
	private int mPreviousKeyCodeIndex = 0;
	private static final int DEFAULT_SLIDE_SENSIVITY_LEVEL = 0;
	private static final int SENSIVITY_LEVELS_AMOUNT = 10;
	private static final int MIN_SLIDE_SENSIVITY = 5;

	private int mSlideSensivity;
	private float mSlideSensivityRegular = 10;
	static LatinKeyboardView mLatinKeyboardView;
	boolean mShowPopup = true;
	
	static enum Directions {
		 UNKNOWN, UP, DOWN, LEFT, RIGHT, UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT;
	}

	@SuppressLint("NewApi")
	public LatinKeyboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mLatinKeyboardView = this;
		mSlideSensivity = getSlideSensivityFromConfig();
		mShowPopup = getShowPopupFromConfig();
		setBackgroundColor(Color.BLACK);
	}

	@SuppressLint("NewApi")
	public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		mLatinKeyboardView = this;
		mShowPopup = getShowPopupFromConfig();
		setBackgroundColor(Color.BLACK);
	}
	
	private int getSlideSensivityFromConfig() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		int sensivityLevelFromConfig =  sharedPreferences.getInt(mContext.getString(R.string.prefer_multikey_sensitivy), DEFAULT_SLIDE_SENSIVITY_LEVEL);
		
		return calculateSensivityFromLevel(sensivityLevelFromConfig);
	}
	
	private int calculateSensivityFromLevel(int sensivityLevelFromConfig) {
		int maxSlideSensivity = (int) (mContext.getResources().getDimension(R.dimen.multi_key_height)/3);
		return (int)(((float)(maxSlideSensivity + MIN_SLIDE_SENSIVITY) / 2) - ((float)((maxSlideSensivity - MIN_SLIDE_SENSIVITY) * sensivityLevelFromConfig) / SENSIVITY_LEVELS_AMOUNT));
	}
	
	public void updateSlideSensivityFromConfig() {
		mSlideSensivity = getSlideSensivityFromConfig();
	}
	
	
	public void resetSlideSensivityToDefault() {
		mSlideSensivity = calculateSensivityFromLevel(DEFAULT_SLIDE_SENSIVITY_LEVEL);
	}
	
	private boolean getShowPopupFromConfig() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		return sharedPreferences.getBoolean(mContext.getString(R.string.prefer_show_popup), true);
	}
	
	public void updateShowPopupFromConfig() {
		mShowPopup = getShowPopupFromConfig();
	}

	public static void setPressedKey(LatinKey pressedKey) {
		LatinKeyboardView.mPressedKey = pressedKey;
	}
	
	public void setPressedKeyCodeIndex(int pressedKeyCodeIndex) {
		mCurrentKeyCodeIndex = pressedKeyCodeIndex;
	}
	
	public static void setIsKeyPressed(boolean isPressed) {
		LatinKeyboardView.mIsKeyPressed = isPressed;
	}
	
	public static boolean getIsKeyPressed() {
		return LatinKeyboardView.mIsKeyPressed;
	}
	
	public static void setSoftKeyboard(SoftKeyboard softKeyboard)  {
		mSoftKeyboard = softKeyboard;
	}
	
	public boolean getShowPopup() {
		return mShowPopup;
	}
	
	public static LatinKeyboardView getLatinKeyboardView() {
		return mLatinKeyboardView;
	}
	
	@SuppressLint("NewApi")
	@Override
	protected boolean onLongPress(Key key) {
		if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
			getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
			return true;
		} 
		return super.onLongPress(key);
	}

	
	@SuppressLint("NewApi")
	@Override
	public boolean onTouchEvent(MotionEvent me) {
		if (!mIsKeyPressed) {
			mStartingX = me.getX();
			mStartingY = me.getY();
		} else {
			int action = me.getActionMasked();

			switch (action) {
			case MotionEvent.ACTION_DOWN:
				break; 
			case MotionEvent.ACTION_MOVE:
				handleActionMove(me);
				return true;
			case MotionEvent.ACTION_UP:
				handleActionUp(me);
				break;
			case MotionEvent.ACTION_CANCEL:
				break;	
			default :
				return true;
			}
		}
		
		return super.onTouchEvent(me);
	}

	public static void setDirectionsPerKey(int codesPerKey) {
		LatinKeyboardView.mCodesPerKey = codesPerKey;
	}
	
	public static int getDirectionsPerKey() {
		return LatinKeyboardView.mCodesPerKey;
	}
	
	private void handleActionUp(MotionEvent me) {
		int keyCode = mPressedKey.codes[0];
		if (mPressedKey.keyType == LatinKey.KEY_TYPES.BIG_SLIDING) {
			keyCode = getMultiKeyCode(me);
		} 
		if (keyCode != 0) {
			mSoftKeyboard.onKeyboardActionListener.onKey(keyCode, null);
			handleSuggestionsOnDelete(keyCode); //Needs to be here because candidates can't be updated while long pressed delete
		}
		
		mCurrentKeyCodeIndex = 0;
		mPreviousKeyCodeIndex = 0;
	}

	private void handleSuggestionsOnDelete(int keyCode) {
		if (keyCode == Keyboard.KEYCODE_DELETE) {
			if (mSoftKeyboard.getSuggestionsOn()) {
				mSoftKeyboard.returnToComposingIfAppropriate();
            }
			if(mSoftKeyboard.getComposingLength() >= 0) {
				mSoftKeyboard.updateCandidates();
			}
		}
	}
	
	private int getMultiKeyCode(MotionEvent me) {
		if (hasMovedOverLimitXorY(me)) {
			double angle = SoftKeyboardHelper.getPositiveAngle(Math.atan2(me.getY() - mStartingY, me.getX() - mStartingX));
			int keyCode = mPressedKey.getKeyCodeIndexFromAngle(angle, mCodesPerKey);
			return mPressedKey.codes[keyCode];
		}
		return mPressedKey.codes[0];
	}

	private boolean hasMovedOverLimitXorY(MotionEvent me) {
		return Math.abs(mStartingX - me.getX()) > mSlideSensivity || Math.abs(mStartingY - me.getY()) > mSlideSensivity;
	}
	
	private void handleActionMove(MotionEvent me) {
		if (mPressedKey.keyType == LatinKey.KEY_TYPES.BIG_SLIDING) {
			if (hasMovedOverLimitXorY(me)) {
				double angle = SoftKeyboardHelper.getPositiveAngle(Math.atan2(me.getY() - mStartingY, me.getX() - mStartingX));
				mCurrentKeyCodeIndex = mPressedKey.getKeyCodeIndexFromAngle(angle, mCodesPerKey);
				if (mCurrentKeyCodeIndex != mPreviousKeyCodeIndex) {
					if(getShowPopup() == true) {
						mPressedKey.updatePopup(mCurrentKeyCodeIndex);
					}
					restartKeyRepeating(mPressedKey.codes[mCurrentKeyCodeIndex]);
					mPreviousKeyCodeIndex = mCurrentKeyCodeIndex;
				}	
			} else {
				if (mPreviousKeyCodeIndex != 0) {
					mCurrentKeyCodeIndex = 0;
					if(getShowPopup() == true) {
						mPressedKey.updatePopup(mCurrentKeyCodeIndex);
					}
					restartKeyRepeating(mPressedKey.codes[mCurrentKeyCodeIndex]);
					mPreviousKeyCodeIndex = mCurrentKeyCodeIndex;
				}
			}
		} 
	}

	private void restartKeyRepeating(int keyCode) {
		mPressedKey.stopKeyRepeat();
		if (mPressedKey.isRepeatable(keyCode)) {
			mPressedKey.startKeyRepeat(keyCode);
		}
	}
	
	protected void showToast(String message) {
		Context context = mContext;
		CharSequence text = message;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
