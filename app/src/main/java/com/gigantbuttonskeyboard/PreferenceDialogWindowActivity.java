package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class PreferenceDialogWindowActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    AlertDialog.Builder builder = initBuilder();

	    AlertDialog alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				dialog.cancel();
                finish();
			}
		});
	    alertDialog.show();
	}

	protected AlertDialog.Builder initBuilder() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(getResources().getString(R.string.default_dialog_message))
		        .setPositiveButton(R.string.default_dialog_positive, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		                Intent intent = new Intent();
		                setResult(1,intent); 
		                dialog.dismiss();
		            }
		        })
		        .setNegativeButton(R.string.default_dialog_negative, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		            	Intent intent = new Intent();
		                setResult(0,intent);
		            	dialog.dismiss();
		            }
		        });
		return builder;
	}
	
}