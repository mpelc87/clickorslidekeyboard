package com.gigantbuttonskeyboard.activity;

import java.util.HashMap;
import java.util.Map;

import com.gigantbuttonskeyboard.R;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.fragment.app.Fragment;

public class HowToUseFragment extends Fragment {
	int currentStepId = 1;
	Map<Integer, Integer> descriptionsMap = new HashMap<Integer, Integer>();
	
	private void initDescriptions() {
		descriptionsMap.put(1, R.string.how_to_use_step_1_description);
		descriptionsMap.put(2, R.string.how_to_use_step_2_description);
		descriptionsMap.put(3, R.string.how_to_use_step_3_description);
		descriptionsMap.put(4, R.string.how_to_use_step_4_description);
		descriptionsMap.put(5, R.string.how_to_use_step_5_description);
		descriptionsMap.put(6, R.string.how_to_use_step_6_description);
		descriptionsMap.put(7, R.string.how_to_use_step_7_description);
		descriptionsMap.put(8, R.string.how_to_use_step_8_description);
		descriptionsMap.put(9, R.string.how_to_use_step_9_description);
		descriptionsMap.put(10, R.string.how_to_use_step_10_description);
	}
	
	private void updateLayout() {
		TextView decription = (TextView)getView().findViewById(R.id.howToUseDescriptionText);
        decription.setText(getText(descriptionsMap.get(currentStepId)));
        
        String gifFileName = "how_to_use_step_" + currentStepId + ".gif";
        
        if(currentStepId > 8 && getResources().getConfiguration().locale.getLanguage().equals("pl")) {
        	gifFileName = "how_to_use_step_" + (currentStepId +1) + ".gif";
        }
        
        WebView webView = (WebView)getView().findViewById(R.id.howToUseWebView);
        String customHtml = 
        		"<html>" +
        				"<body>" +
        				"<img src=\"" + gifFileName + "\" style=\"width:100%\">" +
        				"</body>" +
        		"</html>";
 	    webView.loadDataWithBaseURL("file:///android_asset/", customHtml, "text/html", "utf-8", null);
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.how_to_use_fragment, container, false);
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		LinearLayout titleBar = (LinearLayout) getActivity().findViewById(R.id.configurationTitleBar);
		titleBar.setVisibility(View.GONE);
		
		initDescriptions();     
        final ImageView leftIcon = (ImageView)getView().findViewById(R.id.arrowLeftIcon);
        final ImageView rightIcon = (ImageView)getView().findViewById(R.id.arrowRightIcon);
        
        leftIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentStepId > 1) {
					--currentStepId;
				} 
				
				if(currentStepId < 2) {
					leftIcon.setVisibility(View.INVISIBLE);
				} else if(currentStepId < 9) {
					rightIcon.setVisibility(View.VISIBLE);
				} else if(currentStepId < 10 && getResources().getConfiguration().locale.getLanguage().equals("pl")) {
					rightIcon.setVisibility(View.VISIBLE);
				}
				
				updateLayout();
			}
		});
        
        rightIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentStepId < 9) {
					++currentStepId;
				} else if(currentStepId == 9 && getResources().getConfiguration().locale.getLanguage().equals("pl")) {
					++currentStepId;
				}
				
				leftIcon.setVisibility(View.VISIBLE);
				if(currentStepId > 8 && !getResources().getConfiguration().locale.getLanguage().equals("pl")) {
					rightIcon.setVisibility(View.INVISIBLE);
				} else if(currentStepId > 9) {
					rightIcon.setVisibility(View.INVISIBLE);
				}
				
				updateLayout();
			}
		});
        
        leftIcon.setVisibility(View.INVISIBLE);
        updateLayout();
	}
}
