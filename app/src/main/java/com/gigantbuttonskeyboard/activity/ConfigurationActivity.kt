package com.gigantbuttonskeyboard.activity

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.gigantbuttonskeyboard.R
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds

class ConfigurationActivity : FragmentActivity() {
    private var mAdView: AdView? = null

    private enum class AdViewState {
        NOT_LOADED, LOADING, LOADED
    }

    private var currentAdViewState = AdViewState.NOT_LOADED
    private var networkReceiver: NetworkReceiver? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.configuration_activity)
        val configurationFragment: Fragment = ConfigurationFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.configurationFragmentContainer, configurationFragment)
        transaction.commit()
        MobileAds.initialize(this)
        mAdView = findViewById<View>(R.id.adView) as AdView
        mAdView!!.adListener = object : AdListener() {
            override fun onAdLoaded() {
                currentAdViewState = AdViewState.LOADED
            }

            override fun onAdFailedToLoad(p0: LoadAdError) {
                super.onAdFailedToLoad(p0)
                currentAdViewState = AdViewState.NOT_LOADED
            }
        }
        loadAd()
    }

    public override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            mAdView!!.resume()
        }
        registerNetworkReceiver()
    }

    public override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        unregisterNetworkReceiver()
        super.onPause()
    }

    public override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    private fun registerNetworkReceiver() {
        networkReceiver = NetworkReceiver(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkReceiver, intentFilter)
    }

    private fun unregisterNetworkReceiver() {
        if (networkReceiver != null) {
            unregisterReceiver(networkReceiver)
            networkReceiver = null
        }
    }

    fun loadAd() {
        if (currentAdViewState == AdViewState.NOT_LOADED) {
            currentAdViewState = AdViewState.LOADING
            val adRequest = AdRequest.Builder().build()
            mAdView!!.loadAd(adRequest)
        }
    }
}