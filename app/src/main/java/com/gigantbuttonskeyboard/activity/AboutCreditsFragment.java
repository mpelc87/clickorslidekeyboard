package com.gigantbuttonskeyboard.activity;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.SoftKeyboardHelper;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class AboutCreditsFragment extends Fragment {
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_credits_fragment, container, false);
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		TextView creditsContent = (TextView)getView().findViewById(R.id.creditsContentTextView);
		creditsContent.setText(SoftKeyboardHelper.loadContentFromFileInRaw(getActivity(), R.raw.credits).toString());
	}
}
