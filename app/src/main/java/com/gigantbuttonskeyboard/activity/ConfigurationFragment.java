package com.gigantbuttonskeyboard.activity;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.preferences.ImePreferences;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class ConfigurationFragment extends Fragment {
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.configuration_fragment, container, false);
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		LinearLayout titleBar = (LinearLayout) getActivity().findViewById(R.id.configurationTitleBar);
		titleBar.setVisibility(View.VISIBLE);
		TextView title = (TextView) getActivity().findViewById(R.id.configurationTitle);
		title.setText(getString(R.string.ime_name));
		
		TableRow goToInputPreferences = (TableRow) getView().findViewById(R.id.tableRow1);
        goToInputPreferences.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent goToImePreferences = new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS);
				startActivity(goToImePreferences);
			}
		});
       
        TableRow chooseKeyboard = (TableRow) getView().findViewById(R.id.tableRow2);
        chooseKeyboard.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.showInputMethodPicker();
			}
		});
        
        TableRow keyboardSettings = (TableRow) getView().findViewById(R.id.tableRow3);
        keyboardSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), ImePreferences.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
        
        TableRow howToUse = (TableRow) getView().findViewById(R.id.tableRow4);
        howToUse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment howToUseFragment = new HowToUseFragment();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.addToBackStack(null);
				transaction.replace(R.id.configurationFragmentContainer, howToUseFragment);
				transaction.commit();
			}
		});
        
        TableRow about = (TableRow) getView().findViewById(R.id.tableRow7);
        about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment abountFragment = new AbountFragment();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.addToBackStack(null);
				transaction.replace(R.id.configurationFragmentContainer, abountFragment);
				transaction.commit();
			}
		});
	}
}
