package com.gigantbuttonskeyboard.activity;

import com.gigantbuttonskeyboard.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class AbountFragment extends Fragment {
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_fragment, container, false);
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		TextView title = (TextView) getActivity().findViewById(R.id.configurationTitle);
		title.setText(getString(R.string.about_label));
		
		TableRow terms = (TableRow) getView().findViewById(R.id.aboutTableRow1);
		 terms.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) { 
				Fragment abountTermsFragment = new AboutTermsFragment();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.addToBackStack(null);
				transaction.replace(R.id.configurationFragmentContainer, abountTermsFragment);
				transaction.commit();
			}
			});
	       
	        TableRow credits = (TableRow) getView().findViewById(R.id.aboutTableRow2);
	        credits.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
				Fragment abountCreditsFragment = new AboutCreditsFragment();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.addToBackStack(null);
				transaction.replace(R.id.configurationFragmentContainer, abountCreditsFragment);
				transaction.commit();
			}
			});
	}
}
