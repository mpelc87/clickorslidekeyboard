package com.gigantbuttonskeyboard;


import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.keyboards.KeysDrawables;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.PopupWindow;

public class LatinKey extends Keyboard.Key {
	
	private static final double PI_8 = Math.PI / 8;
	private static final double PI_4 = Math.PI / 4;
	public enum KEY_TYPES {
		SMALL_REGULAR, BIG_SLIDING, BIG_SLIDING_TAPPING;
	}
	public KEY_TYPES keyType = KEY_TYPES.BIG_SLIDING;
	
	int padding = 5;
	private PopupWindow mPreviewPopup = null;
	static Context mSoftKeyboardContext = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext();
	static int mPreviewPopupWitdh = (int)mSoftKeyboardContext.getResources().getDimension(R.dimen.key_height);
	static int mPreviewPopupHeight = (int)mSoftKeyboardContext.getResources().getDimension(R.dimen.key_height);
	Resources mRes;
	private SpecialKeysHandler mSpecKeysHandler = new SpecialKeysHandler(0);
	boolean mIsRepeating = false;
	static SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mSoftKeyboardContext);
	private int rowId = 0;
	private int idInRow = 0;
	
	//NOTE: added orientation to constructor because getting it from resources caused race conditions and sometimes was wrong.
	@SuppressLint("NewApi")
	public LatinKey(Resources res, Keyboard.Row parent, int x, int y, XmlResourceParser parser, int orientation) { 
        super(res, parent, x, y, parser);
        mRes = res;

        if(this.codes.length == 1) {
        	keyType = KEY_TYPES.SMALL_REGULAR;
        }
        
    	if (keyType == KEY_TYPES.SMALL_REGULAR) { //key is small
        	if(orientation == Configuration.ORIENTATION_PORTRAIT) {
        		height = mSharedPreferences.getInt(mSoftKeyboardContext.getString(R.string.prefer_portrait_key_height), (int)res.getDimension(R.dimen.key_height));
        	} else {
        		height = mSharedPreferences.getInt(mSoftKeyboardContext.getString(R.string.prefer_landscape_key_height), (int)res.getDimension(R.dimen.key_height_landscape));
        	}
        } else { //key is big
        	if(orientation == Configuration.ORIENTATION_PORTRAIT) {
        		height = mSharedPreferences.getInt(mSoftKeyboardContext.getString(R.string.prefer_portrait_multikey_height), Math.min(res.getDisplayMetrics().heightPixels/6, (int)res.getDimension(R.dimen.multi_key_height)));
        	} else {
        		height = mSharedPreferences.getInt(mSoftKeyboardContext.getString(R.string.prefer_landscape_multikey_height), (int)res.getDimension(R.dimen.multi_key_height));
        	}
        }
        parent.defaultHeight = height;
    }
	

    /**
     * Overriding this method so that we can reduce the target area for the key that
     * closes the keyboard. 
     */
    @SuppressLint("NewApi")
	@Override
    public boolean isInside(int x, int y) {
        return super.isInside(x, codes[0] == Keyboard.KEYCODE_CANCEL ? y - 10 : y);
    }
    
    @SuppressLint("NewApi")
	@Override
    public void onPressed() {
    	LatinKeyboardView.setIsKeyPressed(true);
    	LatinKeyboardView.setPressedKey(this);
    	LatinKeyboardView.getLatinKeyboardView().setPressedKeyCodeIndex(0);
    	if(LatinKeyboardView.getLatinKeyboardView().getShowPopup() == true) {
    		showPopup(this.codes[0]);
    	}

		if (isRepeatable(this.codes[0])) {
			startKeyRepeat(this.codes[0]);
		}	
    	super.onPressed();
    }
    

	@SuppressLint("NewApi")
	@Override
    public void onReleased(boolean inside) {
    	LatinKeyboardView.setIsKeyPressed(false);
    	if(LatinKeyboardView.getLatinKeyboardView().getShowPopup() == true) {
        	hidePopup();
    	}

    	stopKeyRepeat();
    	
    	super.onReleased(inside);
    }
	
	public void startKeyRepeat(int keyCode) {
		mIsRepeating = true;
		mSpecKeysHandler.setRepeatKeyCode(keyCode);
		mSpecKeysHandler.startRepeat();
    }
    
    public void stopKeyRepeat() {
    	if(mIsRepeating == true) {
    		mSpecKeysHandler.stopRepeat();
    	}
    }
	
    public boolean isRepeatable(int keyCode) {
		if (keyCode == SoftKeyboard.KEYCODE_SPACE || keyCode == Keyboard.KEYCODE_DELETE) {
			return true;
		}
		return false;
    }
    
	@SuppressLint("NewApi")
	public void showPopup(int keyCode) {

		if (LatinKeyboardView.getLatinKeyboardView().isShifted()) {
			keyCode = Character.toUpperCase(keyCode);
        }
		
		Integer drawableInt;
		if(KeysDrawables.getCodesToDrawablesIds().containsKey(keyCode)) {
			drawableInt = KeysDrawables.getCodesToDrawablesIds().get(keyCode);
		} else {
			drawableInt = KeysDrawables.getCodesToDrawablesIds().get(0);
		}

		ImageView imageView = null;
		if(mPreviewPopup == null) {
			mPreviewPopup = new PopupWindow(SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext());
			mPreviewPopup.setTouchable(false);
			mPreviewPopup.setWidth(mPreviewPopupWitdh);
			mPreviewPopup.setHeight(mPreviewPopupHeight);
			imageView = new ImageView(SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext());
		} else {
			imageView = ((ImageView)mPreviewPopup.getContentView());
		}
		
		Drawable drawable = mRes.getDrawable(drawableInt);
		imageView.setImageDrawable(drawable);
		mPreviewPopup.setContentView(imageView);
		
		int previewPopupX, previewPopupY;
		
	    if(keyType != KEY_TYPES.SMALL_REGULAR) {
            previewPopupX = this.x + this.width/4;
        } else {
            previewPopupX = this.x - mPreviewPopupWitdh/2 + this.width/2;
        }
        previewPopupY = this.y - this.height/2;
        
        SoftKeyboard softKeyboard = SoftKeyboard.Companion.getSoftKeyboard();
        int orientation = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext().getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE && softKeyboard.isFullscreenMode()) {
            previewPopupY+= softKeyboard.getInputInFullScreenHeight();
//            if(SoftKeyboard.getSoftKeyboard().getAdViewActive()) {
//        		previewPopupY -= softKeyboard.getAdView().getHeight();
//        	}
        }
            
        mPreviewPopup.showAtLocation(LatinKeyboardView.getLatinKeyboardView(), Gravity.NO_GRAVITY, previewPopupX, previewPopupY);
        imageView.setVisibility(LatinKeyboardView.VISIBLE);
	}
	
	void hidePopup() {
		if(mPreviewPopup.isShowing()) {
			mPreviewPopup.dismiss();
		}
	}
	
	@SuppressLint("NewApi")
	void updatePopup(int keyCodeIndex) {
		
		if(mPreviewPopup.isShowing()) {
			int keyCode = this.codes[keyCodeIndex];
			if (LatinKeyboardView.getLatinKeyboardView().isShifted()) {
				keyCode = Character.toUpperCase(keyCode);
	        }
			
			Integer drawableInt;
			if(KeysDrawables.getCodesToDrawablesIds().containsKey(keyCode)) {
				drawableInt = KeysDrawables.getCodesToDrawablesIds().get(keyCode);
			} else {
				drawableInt = KeysDrawables.getCodesToDrawablesIds().get(0);
			}
			Drawable drawable = mRes.getDrawable(drawableInt);

			((ImageView)mPreviewPopup.getContentView()).setImageDrawable(drawable);
			int previewPopupX = 0; 
			int previewPopupY = 0;
			SoftKeyboard softKeyboard = SoftKeyboard.Companion.getSoftKeyboard();
			
				//direction UP       //direction UP-LEFT  //direction UP-RIGHT
			if (keyCodeIndex == 1 || keyCodeIndex == 5 || keyCodeIndex == 6) {
				previewPopupX = this.x + this.width/4;
				previewPopupY = this.y + this.height + this.height/2;
				
				if((this.y + this.height + mPreviewPopupHeight) > LatinKeyboardView.getLatinKeyboardView().getBottom()) {
		        	previewPopupY = LatinKeyboardView.getLatinKeyboardView().getBottom() - mPreviewPopupHeight/4;
		        }
			} else {
				previewPopupX = this.x + this.width/4;
				previewPopupY = this.y - this.height/2;
			}
			
			int orientation = SoftKeyboard.Companion.getSoftKeyboard().getSoftKeyboardContext().getResources().getConfiguration().orientation;
	        if(orientation == Configuration.ORIENTATION_LANDSCAPE && softKeyboard.isFullscreenMode()) {
	            previewPopupY+= softKeyboard.getInputInFullScreenHeight();
//	            if(SoftKeyboard.getSoftKeyboard().getAdViewActive()) {
//	        		previewPopupY -= softKeyboard.getAdView().getHeight();
//	        	}
	        }
			mPreviewPopup.update(previewPopupX, previewPopupY, mPreviewPopupWitdh, mPreviewPopupHeight);
		}
	}
	public void setRowId(int id) {
		this.rowId = id;
	}
	
	public int getRowId() {
		return rowId;
	}
	
	public void setIdInRow(int id) {
		this.idInRow = id;
	}
	
	public int getIdInRow() {
		return idInRow;
	}
	
	public int getKeyCodeIndexFromAngle(double angle, int codesPerKey) {
		if(codesPerKey == 5) {
			return get4KeyCodeIndexFromAngle(angle);
		}
		return get8KeyCodeIndexFromAngle(angle);
	}
	
	public int get4KeyCodeIndexFromAngle(double angle) {
		if (angle >= PI_4 && angle < 3 * PI_4) {
			return 3;
		} else if (angle >= 3 * PI_4 && angle < 5 * PI_4) {
			return 2;
		} else if (angle >= 5 * PI_4 && angle < 7 * PI_4) {
			return 1;
		} else if ((angle >= 0 && angle < PI_4)
				|| (angle >= 7 * PI_4 && angle < 2 * Math.PI)) {
			return 4;
		}
		return 0;
	}
	
	public int get8KeyCodeIndexFromAngle(double angle) {
		if (angle >= PI_8 && angle < 3 * PI_8) {
			if(this.codes[8] == 0) {
				if (angle >= PI_4 && angle < 3 * PI_4) {
					return 3;
				} else if ((angle >= 0 && angle < PI_4)
						|| (angle >= 7 * Math.PI / 4 && angle < 2 * Math.PI)) {
					return 4;
				}
			}
			return 8;
		} else if (angle >= 3 * PI_8 && angle < 5 * PI_8) {
			return 3;
		} else if (angle >= 5 * PI_8 && angle < 7 * PI_8) {
			if(this.codes[7] == 0) {
				if (angle >= PI_4 && angle < 3 * PI_4) {
					return 3;
				} else if (angle >= 3 * PI_4 && angle < 5 * PI_4) {
					return 2;
				}
			}
			return 7;
		} else if (angle >= 7 * PI_8 && angle < 9 * PI_8) {
			return 2;
		} else if (angle >= 9 * PI_8 && angle < 11 * PI_8) {
			if(this.codes[5] == 0) {
				if (angle >= 3 * PI_4 && angle < 5 * PI_4) {
					return 2;
				} else if (angle >= 5 * PI_4 && angle < 7 * PI_4) {
					return 1;
				} 
			}
			return 5;
		} else if (angle >= 11 * PI_8 && angle < 13 * PI_8) {
			return 1;
		} else if (angle >= 13 * PI_8 && angle < 15 * PI_8) {
			if(this.codes[6] == 0) {
				if (angle >= 5 * PI_4 && angle < 7 * PI_4) {
					return 1;
				} else if ((angle >= 0 && angle < PI_4)
						|| (angle >= 7 * Math.PI / 4 && angle < 2 * Math.PI)) {
					return 4;
				}
			}
			return 6;
		} else if ((angle >= 0 && angle < PI_8)
				|| (angle >= 15 * Math.PI / 8 && angle < 2 * Math.PI)) {
			return 4;
		}
		return 0;
	}
	
}