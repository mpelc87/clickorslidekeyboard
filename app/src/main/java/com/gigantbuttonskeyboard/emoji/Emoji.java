package com.gigantbuttonskeyboard.emoji;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.util.Log;
import android.view.ViewGroup;
import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.SoftKeyboard;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Emoji {

	private static final String [] EMOJIS = new String[] {

	};
	private static final String EMOJI_SMILES = "emoji_1_smiles";
	private static final String EMOJI_ANIMALS = "emoji_2_animals";
	private static final String EMOJI_PLANTS = "emoji_3_plants";
	private static final String EMOJI_TRANSPORT = "emoji_4_transport";
	private static final String EMOJI_TRAVEL = "emoji_5_travel";
	private static final String EMOJI_FOOD = "emoji_6_food";
	private static final String EMOJI_BODY_CLOTHES = "emoji_7_body_clothes";
	private static final String EMOJI_GAME = "emoji_8_game";
	private static final String EMOJI_MEDIA = "emoji_9_media";
	private static final String EMOJI_OFFICE = "emoji_10_office";
	private static final String EMOJI_TOOLS = "emoji_11_tools";
	private static final String EMOJI_WEATHER = "emoji_12_weather";

	private Map<String, EmojiAdapter> mEmojiAdapters;

	private View mEmojiView;
	private SoftKeyboard mSoftKeyboard;

	private Map initEmojiAdapters(Context context) {
		Map<String, EmojiAdapter> emojiAdapters = new HashMap<>();
		emojiAdapters.put(EMOJI_SMILES, new EmojiAdapter(context, EMOJI_SMILES));
		emojiAdapters.put(EMOJI_ANIMALS, new EmojiAdapter(context, EMOJI_ANIMALS));
		emojiAdapters.put(EMOJI_PLANTS, new EmojiAdapter(context, EMOJI_PLANTS));
		emojiAdapters.put(EMOJI_TRANSPORT, new EmojiAdapter(context, EMOJI_TRANSPORT));
		emojiAdapters.put(EMOJI_TRAVEL, new EmojiAdapter(context, EMOJI_TRAVEL));
		emojiAdapters.put(EMOJI_FOOD, new EmojiAdapter(context, EMOJI_FOOD));
		emojiAdapters.put(EMOJI_BODY_CLOTHES, new EmojiAdapter(context, EMOJI_BODY_CLOTHES));
		emojiAdapters.put(EMOJI_GAME, new EmojiAdapter(context, EMOJI_GAME));
		emojiAdapters.put(EMOJI_MEDIA, new EmojiAdapter(context, EMOJI_MEDIA));
		emojiAdapters.put(EMOJI_OFFICE, new EmojiAdapter(context, EMOJI_OFFICE));
		emojiAdapters.put(EMOJI_TOOLS, new EmojiAdapter(context, EMOJI_TOOLS));
		emojiAdapters.put(EMOJI_WEATHER, new EmojiAdapter(context, EMOJI_WEATHER));

		return emojiAdapters;
	}

	public Emoji(SoftKeyboard softKeyboard) {
		mSoftKeyboard = softKeyboard;
		mEmojiView = mSoftKeyboard.getLayoutInflater().inflate(R.layout.emoji_input, null);
		mEmojiAdapters = initEmojiAdapters(softKeyboard.getBaseContext());

		ImageView settingsKey = (ImageView)mEmojiView.findViewById(R.id.emojiSettingsKeyImageView);
		settingsKey.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSoftKeyboard.onEmojiKeyPress(SoftKeyboard.KEYCODE_SETTINGS);
			}
		});

		ImageView switchToKeyboard = (ImageView)mEmojiView.findViewById(R.id.emojiSwitchToKeyboardKeyImageView);
		switchToKeyboard.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mSoftKeyboard.setKeyboardViewTypeToKeyboard();
			}
		});

		ImageView spaceKey = (ImageView)mEmojiView.findViewById(R.id.emojiSpaceKeyImageView);
		spaceKey.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSoftKeyboard.onEmojiKeyPress(SoftKeyboard.KEYCODE_SPACE);
			}
		});

		ImageView enterKey = (ImageView)mEmojiView.findViewById(R.id.emojiEnterKeyImageView);
		enterKey.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSoftKeyboard.onEmojiKeyPress(SoftKeyboard.CODE_ENTER);
			}
		});

		ImageView deleteKey = (ImageView)mEmojiView.findViewById(R.id.emojiDeleteKeyImageView);
		deleteKey.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSoftKeyboard.onEmojiKeyPress(Keyboard.KEYCODE_DELETE);
			}
		});
		
		updateEmojiGridHeight((int)softKeyboard.getBaseContext().getResources().getDimension(R.dimen.emoji_grid_height));

		final GridView emojiContainerGrid = (GridView) mEmojiView.findViewById(R.id.emojiContainerContentGridView);
		emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_SMILES));
		emojiContainerGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				mSoftKeyboard.onEmojiPress(((TextView)view).getText());
			}
		});
		
		TextView tab1 = (TextView) mEmojiView.findViewById(R.id.emoji1TabTextView);
		tab1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_SMILES));
			}
		});
		
		TextView tab2 = (TextView) mEmojiView.findViewById(R.id.emoji2TabTextView);
		tab2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_ANIMALS));
			}
		});

		TextView tab3 = (TextView) mEmojiView.findViewById(R.id.emoji3TabTextView);
		tab3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_PLANTS));
			}
		});

		TextView tab4 = (TextView) mEmojiView.findViewById(R.id.emoji4TabTextView);
		tab4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_TRANSPORT));
			}
		});

		TextView tab5 = (TextView) mEmojiView.findViewById(R.id.emoji5TabTextView);
		tab5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_TRAVEL));
			}
		});

		TextView tab6 = (TextView) mEmojiView.findViewById(R.id.emoji6TabTextView);
		tab6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_FOOD));
			}
		});

		TextView tab7 = (TextView) mEmojiView.findViewById(R.id.emoji7TabTextView);
		tab7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_BODY_CLOTHES));
			}
		});

		TextView tab8 = (TextView) mEmojiView.findViewById(R.id.emoji8TabTextView);
		tab8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_GAME));
			}
		});

		TextView tab9 = (TextView) mEmojiView.findViewById(R.id.emoji9TabTextView);
		tab9.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_MEDIA));
			}
		});

		TextView tab10 = (TextView) mEmojiView.findViewById(R.id.emoji10TabTextView);
		tab10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_OFFICE));
			}
		});

		TextView tab11 = (TextView) mEmojiView.findViewById(R.id.emoji11TabTextView);
		tab11.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_TOOLS));
			}
		});

		TextView tab12 = (TextView) mEmojiView.findViewById(R.id.emoji12TabTextView);
		tab12.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emojiContainerGrid.setAdapter(mEmojiAdapters.get(EMOJI_WEATHER));
			}
		});
	}

	public void updateEmojiGridHeight(int height) {
		final GridView emojiContainerGrid = (GridView) mEmojiView.findViewById(R.id.emojiContainerContentGridView);
		ViewGroup.LayoutParams gridParams = emojiContainerGrid.getLayoutParams();
		gridParams.height = height;
		emojiContainerGrid.setLayoutParams(gridParams);
	}
	
	public View getEmojiView() {
		return mEmojiView;
	}
	
}
