package com.gigantbuttonskeyboard.emoji;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.gigantbuttonskeyboard.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EmojiAdapter extends BaseAdapter {
	private Context mContext;
	protected static int emojiAmount;
	protected List<CharSequence> mEmojiIds;

	public EmojiAdapter(Context context, String fileName) {
		mContext = context;
		mEmojiIds = generateEmojiIds(context, fileName);
	}

	protected static List<CharSequence> generateEmojiIds(Context context, String fileName) {
		List emojis= new ArrayList<CharSequence>(50);

		BufferedReader emojiFileBufferedReader = null;
		try {
			emojiFileBufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));

			String line;
			while ((line = emojiFileBufferedReader.readLine()) != null) {
				emojis.add(line);
			}
		} catch (IOException e) {
		} finally {
			if (emojiFileBufferedReader != null) {
				try {
					emojiFileBufferedReader.close();
				} catch (IOException e) {
				}
			}
		}

		return emojis;
	}

	@Override
	public int getCount() {
		return mEmojiIds.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView;
		if(convertView == null) {
			textView = new TextView(mContext);
			textView.setTextColor(Color.WHITE);
			textView.setTextSize(mContext.getResources().getDimension(R.dimen.emoji_icons_font));
			textView.setGravity(Gravity.CENTER_HORIZONTAL);
		} else {
			textView = (TextView) convertView;
		}
		
		textView.setText(mEmojiIds.get(position));
		return textView;
	}
}
