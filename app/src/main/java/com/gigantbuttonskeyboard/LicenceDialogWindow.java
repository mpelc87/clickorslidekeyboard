package com.gigantbuttonskeyboard;

import java.util.List;

import com.gigantbuttonskeyboard.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

public class LicenceDialogWindow extends Activity {

	private AlertDialog alertDialog;
	
	private SharedPreferences sharedPreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	    
	    AlertDialog.Builder builder = initBuilder();
	    alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				dialog.cancel();
                finish();
			}
		});
	    alertDialog.show();
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private AlertDialog.Builder initBuilder() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View alertDialogView = getLayoutInflater().inflate(R.layout.licence_dialog, null);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
	    	Point size = new Point();
		    getWindowManager().getDefaultDisplay().getSize(size);
		    alertDialogView.setMinimumWidth((int)(size.x * 0.9));
	    } else {
	    	alertDialogView.setMinimumWidth((int)(getWindowManager().getDefaultDisplay().getWidth() * 0.9));
	    }
		
		View buttonsLayout = (LinearLayout) alertDialogView.findViewById(R.id.licence_acceptance_buttons_layout);
		LinearLayout.LayoutParams buttonsLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		buttonsLayoutParams.gravity = Gravity.CENTER;
		buttonsLayout.setLayoutParams(buttonsLayoutParams);
		
		EditText licenceContent = (EditText)alertDialogView.findViewById(R.id.licence_acceptance_editText);
		licenceContent.setText(loadLicenceAndCredits());
		
	    builder.setView(alertDialogView);
	    
	    CheckBox checkbox = (CheckBox) alertDialogView.findViewById(R.id.licence_acceptance_checkbox);
	    
	    final Button acceptButton = (Button) alertDialogView.findViewById(R.id.licence_acceptance_accept_button);
	    acceptButton.setEnabled(checkbox.isChecked());
	    acceptButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				saveLicenceAcceptanceToSharedPreferences();
				Intent intent = new Intent(LicenceDialogWindow.this, TutorialDialogWindow.class);
				startActivity(intent);
				alertDialog.dismiss();
			}
		});
	    
	    checkbox.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CheckBox checkbox = (CheckBox)v;
				acceptButton.setEnabled(checkbox.isChecked());
			}
		});
	    
	    Button declineButton = (Button) alertDialogView.findViewById(R.id.licence_acceptance_decline_button);
	    declineButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
	    
	    return builder;
	}
	
	private String loadLicenceAndCredits() {
		StringBuilder licenceContent = SoftKeyboardHelper.loadContentFromFileInRaw(this.getBaseContext(), R.raw.terms);
		StringBuilder creditsContent = SoftKeyboardHelper.loadContentFromFileInRaw(this.getBaseContext(), R.raw.credits);
		
		return licenceContent.append(creditsContent).toString();
	}
	
	
	private void saveLicenceAcceptanceToSharedPreferences() {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(getResources().getString(R.string.prefer_licence_acceptance), true);
		editor.apply();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if(sharedPreferences.getBoolean(getString(R.string.prefer_licence_acceptance),false) == false) {
			switchToDifferentIme();
    	}
	}
	
	@SuppressLint("NewApi")
	private void switchToDifferentIme() {
		SoftKeyboard softkeyboard = SoftKeyboard.Companion.getSoftKeyboard();
		InputMethodManager inputMethodManager = (InputMethodManager) softkeyboard.getSystemService(Context.INPUT_METHOD_SERVICE);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) { 
	    	inputMethodManager.switchToLastInputMethod(softkeyboard.getWindow().getWindow().getAttributes().token);
		} else {
			List<InputMethodInfo> imeList = inputMethodManager.getEnabledInputMethodList();
			if(!imeList.isEmpty()) {
				InputMethodInfo ime = imeList.get(0);
				String id = ime.getId();
				inputMethodManager.setInputMethod(softkeyboard.getWindow().getWindow().getAttributes().token, id);
			}
		}
    	
	}
}