package com.gigantbuttonskeyboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;


public class SoftKeyboardHelper {
	
	public static final String ABOUT_INFORMATION_TYPE = "about_information_type";
	public static final String ABOUT_INFORMATION_TERMS = "terms";
	public static final String ABOUT_INFORMATION_CREDITS = "credits";
	public static final String ABOUT_INFORMATION_PRO = "pro";
	
	public static double getPositiveAngle(double atan2) {
		if (atan2 < 0) {
			atan2 = atan2 + 2 * Math.PI;
		}
		return atan2;
	}
	
	/**
     * Helper to determine if a given character code is alphabetic.
     */
    public static boolean isAlphabet(int code) {
        if (Character.isLetter(code)) {
            return true;
        }
		return false;
    }
    
    public static boolean isWordSeparator(int keyCode) {
        //adding codes for special keys e.g. shift etc .
    	String specialChars = String.valueOf((char)-1);
        specialChars += String.valueOf((char)-2);
        specialChars += String.valueOf((char)-22);
        specialChars += String.valueOf((char)-5);
        specialChars += String.valueOf((char)10);
        specialChars += String.valueOf((char)-100);
        specialChars += String.valueOf((char)-101);
        specialChars += String.valueOf((char)-102);
        specialChars += String.valueOf((char)-103);
        specialChars += String.valueOf((char)-104);
        specialChars += String.valueOf((char)-105);
        return (!Character.isLetter(keyCode) && !specialChars.contains(String.valueOf((char)keyCode)));
    }
    
    public static boolean isDpadKey(int keyCode) {
    	return (keyCode > 18 && keyCode < 23);
    }
    
    public static StringBuilder loadContentFromFileInRaw(Context context, int rawResId) {
		StringBuilder licenceContent = new StringBuilder();
		try {
			InputStream inputStream = context.getResources().openRawResource(rawResId);
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		    String line;
		    String separator = System.getProperty("line.separator");
		    while ((line = bufferedReader.readLine()) != null) {
		    	licenceContent.append(line);
		    	licenceContent.append(separator);
		    }
		    bufferedReader.close();
		}
		catch (IOException e) {
		} 
		
		return licenceContent;
	}
}
