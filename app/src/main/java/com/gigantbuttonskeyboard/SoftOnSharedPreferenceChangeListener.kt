package com.gigantbuttonskeyboard

import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.preference.PreferenceManager
import com.gigantbuttonskeyboard.keyboards.KeysDrawables
import com.gigantbuttonskeyboard.keyboards.PortraitKeyboardsGroup
import com.gigantbuttonskeyboard.suggestions.SuggestionsHelper

class SoftOnSharedPreferenceChangeListener(private val softKeyboard: SoftKeyboard) : SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onSharedPreferenceChanged(
        prefs: SharedPreferences,
        sharedPreferenceKey: String?
    ) {
        if (sharedPreferenceKey == null) return
        with(softKeyboard) {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
            if (sharedPreferenceKey == getString(R.string.prefer_suggestions_functionality)) {
                if (!sharedPreferences!!.getBoolean(sharedPreferenceKey, true)) {
                    stopGenerationIfInProgress()
                }
            } else if (sharedPreferenceKey == getString(R.string.global_last_reset_time)) {
                handleResetToDefaults(sharedPreferences)
            } else if (sharedPreferenceKey == getString(R.string.current_suggestions_last_reset_time)) {
                destroySuggestions()
                SuggestionsHelper.resetCurrentDictionaries(softKeyboardContext)
            } else if (sharedPreferenceKey == getString(R.string.all_suggestions_last_reset_time)) {
                destroySuggestions()
                SuggestionsHelper.resetAllDictionaries(softKeyboardContext)
            } else if (sharedPreferenceKey == getString(R.string.prefer_suggestions_word_amount_percents)) {
                destroySuggestions()
            } else if (sharedPreferenceKey == getString(R.string.keyboard_current_language)) {
                handleLanguageChange(sharedPreferences, sharedPreferenceKey)
            } else if (sharedPreferenceKey == getString(R.string.prefer_suggestions_candidates_amount)) {
                SoftKeyboard.suggestionsLimit =
                    sharedPreferences!!.getInt(sharedPreferenceKey,
                        SoftKeyboard.DEFAULT_SUGGESTIONS_LIMIT
                    )
            } else if (sharedPreferenceKey == getString(R.string.prefer_multikey_sensitivy)) {
                if (inputView != null) {
                    inputView!!.updateSlideSensivityFromConfig()
                }
            } else if (sharedPreferenceKey == getString(R.string.prefer_show_popup)) {
                if (inputView != null) {
                    inputView!!.updateShowPopupFromConfig()
                }
            } else if (sharedPreferenceKey.contains("_multikey_height")) {
                handleMultiKeyHeightChange(sharedPreferenceKey)
            } else if (sharedPreferenceKey.contains("_key_height") || sharedPreferenceKey.contains(
                    "botton_padding_height"
                )
            ) {
                handleKeyHeightPaddingChange()
            } else if (sharedPreferenceKey.contains("candidates_dimens")) {
                handleCandidatesDimensChange(sharedPreferences)
            } else if (sharedPreferenceKey == getString(R.string.prefer_keep_accents)) {
                if (currentKeyboardLang == SoftKeyboard.KeyboardLanguages.PL) {
                    keepLetterAccent = sharedPreferences!!.getBoolean(
                        getString(R.string.prefer_keep_accents),
                        true
                    )
                }
            } else if (sharedPreferenceKey == getString(R.string.prefer_multi_letter_layout)) {
                handleMultiKeyLayoutChange(sharedPreferenceKey)
            } else if (sharedPreferenceKey == getString(R.string.prefer_qwerty_layout)) {
                handleRegularLayoutChange(softKeyboard)
            }
        }

    }

    private fun handleMultiKeyLayoutChange(sharedPreferenceKey: String) {
        handleMultiKeyHeightChange(sharedPreferenceKey)
    }

    private fun handleRegularLayoutChange(softKeyboard: SoftKeyboard) {
        with(softKeyboard) {
            updateCurrentLayouts()
            destroyKeyboards()
            makeKeyboardsIfNull()
        }
    }

    private fun handleMultiKeyHeightChange(sharedPreferenceKey: String) {
        with(softKeyboard) {
            updateCurrentLayouts()
            destroyKeyboards()
            if (sharedPreferenceKey == getString(R.string.prefer_portrait_multikey_height)) {
                KeysDrawables.reInitPortraitBitmapDrawablesMapping()
            } else {
                KeysDrawables.reInitLandscapeBitmapDrawablesMapping()
            }
            makeKeyboardsIfNull()
        }
    }

    private fun handleKeyHeightPaddingChange() {
        with(softKeyboard) {
            updateCurrentLayouts()
            destroyKeyboards()
            makeKeyboardsIfNull()
        }
    }

    private fun handleCandidatesDimensChange(sharedPreferences: SharedPreferences?) {
        with (softKeyboard) {
            if (candidateView != null) {
                val currentCandidateHeight = sharedPreferences!!.getInt(
                    getString(R.string.prefer_suggestions_candidates_dimens_total_height),
                    (resources.getDimension(R.dimen.candidate_font_height) + resources.getDimensionPixelSize(
                        R.dimen.candidate_vertical_padding
                    )).toInt()
                )
                val candidateFontSizeInPercents = sharedPreferences.getInt(
                    getString(R.string.prefer_suggestions_candidates_dimens_font_size),
                    resources.getInteger(R.integer.candidate_font_height_percent)
                )
                val candidateFontSizeReduction =
                    resources.getDimensionPixelSize(R.dimen.candidate_font_height_percent_reduction)
                candidateView!!.setCandidateFontHeight((candidateFontSizeInPercents.toFloat() / 100 * currentCandidateHeight).toInt() - candidateFontSizeReduction)
                candidateView!!.setVerticalPadding(((1 - candidateFontSizeInPercents.toFloat() / 100) * currentCandidateHeight).toInt() + candidateFontSizeReduction)
            }
        }
    }

    private fun handleLanguageChange(
        sharedPreferences: SharedPreferences?,
        sharedPreferenceKey: String
    ) {
        with (softKeyboard) {
            val currentKeyboardLanguage = sharedPreferences!!.getString(
                sharedPreferenceKey,
                getString(R.string.default_keyboard_language)
            )
            currentKeyboardLang = LanguagesHelper.getLangEnum(currentKeyboardLanguage)
            updateLocale(currentKeyboardLanguage)
            updateCurrentLayouts()
            destroyKeyboards()
            destroySuggestions()
            SuggestionsHelper.setUpDictionaries(softKeyboardContext)
            makeKeyboardsIfNull()
        }
    }

    private fun handleResetToDefaults(sharedPreferences: SharedPreferences?) {
        with (softKeyboard) {
            val wasLicenceAccepted = sharedPreferences!!.getBoolean(
                getString(R.string.prefer_licence_acceptance),
                false
            )
            var editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
            updateLocale(deviceLanguage)
            updateCurrentLayouts()
            destroyKeyboards()
            destroySuggestions()
            SuggestionsHelper.resetAllDictionaries(softKeyboardContext)
            SuggestionsHelper.setUpDictionaries(softKeyboardContext)
            makeKeyboardsIfNull()
            editor = sharedPreferences.edit()
            editor.putBoolean(
                resources.getString(R.string.prefer_licence_acceptance),
                wasLicenceAccepted
            )
            var currentVersionCode = 0
            try {
                currentVersionCode = packageManager.getPackageInfo(packageName, 0).versionCode
            } catch (e: PackageManager.NameNotFoundException) {
            }
            editor.putString(
                softKeyboardContext!!.getString(R.string.prefer_multi_letter_layout),
                PortraitKeyboardsGroup.MultiKeyLayout.OLD_3_X_4.ordinal.toString()
            )
            editor.putInt(getString(R.string.app_current_version_code_key), currentVersionCode)
            editor.apply()
        }
    }
}