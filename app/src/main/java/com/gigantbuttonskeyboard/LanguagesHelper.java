package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.SoftKeyboard.KeyboardLanguages;

public class LanguagesHelper {
	private static final String CODE_EN = "en";
	private static final String CODE_DE = "de";
	private static final String CODE_PL = "pl";

	
	public static KeyboardLanguages getLangEnum(String currentKeyboardLanguage) {
		if(currentKeyboardLanguage.equals(CODE_PL)) {
			return KeyboardLanguages.PL;
		} else if (currentKeyboardLanguage.equals(CODE_DE)){
			return KeyboardLanguages.DE;
		} else {
			return KeyboardLanguages.EN;
		}
	}
	
	public static String getLangCode(String systemLanguage) {
		if(systemLanguage.equals(CODE_PL) || systemLanguage.equals(CODE_DE)) {
			return systemLanguage;
		}  
		return CODE_EN;
	}
}
