package com.gigantbuttonskeyboard

import android.inputmethodservice.Keyboard
import android.os.Handler
import android.os.Message
import com.gigantbuttonskeyboard.SoftKeyboard.Companion.softKeyboard

class SpecialKeysHandler(private var mKeyCode: Int) : Handler() {
    private var mRepeatStartTime: Long = 0
    fun setRepeatKeyCode(keyCode: Int) {
        mKeyCode = keyCode
    }

    override fun handleMessage(msg: Message) { //copied from KeyboardView.java
        when (msg.what) {
            MSG_REPEAT -> if (repeatKey()) {
                val repeat = Message.obtain(this, MSG_REPEAT)
                sendMessageDelayed(repeat, REPEAT_INTERVAL.toLong())
            }
        }
    }

    private fun repeatKey(): Boolean {
        if (mKeyCode != 0) {
            if (mKeyCode == Keyboard.KEYCODE_DELETE) {
                val timeFromRepeatInSeconds = timeFromReapeatStartInseconds
                val amountOfCharactersToMultiplicate =
                    (if (timeFromRepeatInSeconds > REAPEAT_MULTIPLICATION_THRESHOLD) timeFromRepeatInSeconds else 1)
                if (amountOfCharactersToMultiplicate == 1) {
                    softKeyboard!!.onKeyboardActionListener.onKey(mKeyCode, null)
                } else {
                    softKeyboard!!.handleFastBackspace(amountOfCharactersToMultiplicate)
                }
            } else {
                softKeyboard!!.onKeyboardActionListener.onKey(mKeyCode, null)
            }
        }
        return true
    }

    private val timeFromReapeatStartInseconds: Int
        private get() = ((System.currentTimeMillis() - mRepeatStartTime) / 1000).toInt()

    fun startRepeat() {
        mRepeatStartTime = System.currentTimeMillis()
        val msg = this.obtainMessage(MSG_REPEAT)
        sendMessageDelayed(msg, REPEAT_START_DELAY.toLong())
    }

    fun stopRepeat() {
        this.removeMessages(MSG_REPEAT)
    }

    companion object {
        private const val MSG_REPEAT = 3
        private const val REPEAT_INTERVAL = 50 // ~20 keys per second
        private const val REPEAT_START_DELAY = 400
        private const val REAPEAT_MULTIPLICATION_THRESHOLD = 2
    }
}