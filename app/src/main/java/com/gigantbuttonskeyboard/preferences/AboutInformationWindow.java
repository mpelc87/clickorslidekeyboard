package com.gigantbuttonskeyboard.preferences;

import com.gigantbuttonskeyboard.R;
import com.gigantbuttonskeyboard.SoftKeyboardHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class AboutInformationWindow extends Activity {
	String title = "";
	String content = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    Intent intent = getIntent();
	    final String informationType = intent.getStringExtra(SoftKeyboardHelper.ABOUT_INFORMATION_TYPE);
	    
	    if(informationType.equals(SoftKeyboardHelper.ABOUT_INFORMATION_TERMS)) {
	    	title = getString(R.string.prefer_terms_title);
	    	content = SoftKeyboardHelper.loadContentFromFileInRaw(this.getBaseContext(), R.raw.terms).toString();
	    } else if(informationType.equals(SoftKeyboardHelper.ABOUT_INFORMATION_CREDITS)) {
	    	title = getString(R.string.prefer_credits_title);
	    	content = SoftKeyboardHelper.loadContentFromFileInRaw(this.getBaseContext(), R.raw.credits).toString();
	    } else if(informationType.equals(SoftKeyboardHelper.ABOUT_INFORMATION_PRO)) {
	    	title = getString(R.string.prefer_information_pro_title);
	    	content = getString(R.string.prefer_information_pro_desc);
	    }
	    
	    AlertDialog.Builder builder = initBuilder();
	    AlertDialog alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				dialog.cancel();
                finish();
			}
		});
	    alertDialog.show();
	}

	private AlertDialog.Builder initBuilder() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(title)
	    	.setMessage(content)
	    	.setPositiveButton(getResources().getString(R.string.default_dialog_ok), new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		                dialog.dismiss();
		            }
		        });
		return builder;
	}
	
}