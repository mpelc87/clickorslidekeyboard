/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gigantbuttonskeyboard.preferences;

import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

import com.gigantbuttonskeyboard.*;

/**
 * Displays the IME preferences inside the input method setting.
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ImePreferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
	static ImePreferences mImePreference;
	
    @Override
    public Intent getIntent() {
        final Intent modIntent = new Intent(super.getIntent());
        modIntent.putExtra(EXTRA_SHOW_FRAGMENT, Settings.class.getName());
        modIntent.putExtra(EXTRA_NO_HEADERS, true);
        return modIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImePreference = this;
        setTitle(R.string.settings_name);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected boolean isValidFragment(String fragmentName) {
    	  return Settings.class.getName().equals(fragmentName);
    }
    
    public static class Settings extends InputMethodSettingsFragment {
    	private static final int RESET_GLOBAL_REQ = 1;
        private static final int RESET_ALL_SUGGESTIONS_REQ = 3;
		private static final int RESET_CURRENT_SUGGESTIONS_REQ = 2;
		private static final int RESET_POSITIVE_RESULT_CODE = 1;
		
		SharedPreferences mSharedPreferences;

		@SuppressLint("ResourceType")
		@Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            setInputMethodSettingsCategoryTitle(R.string.language_selection_title);
            setSubtypeEnablerTitle(R.string.select_language);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.layout.ime_preferences);
            
            final ListPreference keyboardUserLanguagePreference = (ListPreference)findPreference(getString(R.string.prefer_keyboard_user_language));
            if(keyboardUserLanguagePreference.getValue() == null || keyboardUserLanguagePreference.getValue().equals("")) {
            	keyboardUserLanguagePreference.setValueIndex(0);
            }
            keyboardUserLanguagePreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
				@Override
				public boolean onPreferenceChange(Preference userLanguagePreference, Object newValue) {
					String newUserLanguage = (String)newValue;
					if(newUserLanguage.equals("auto")) {
						String systemLanguage = SoftKeyboard.Companion.getSoftKeyboard().getDeviceLanguage();
						newUserLanguage = LanguagesHelper.getLangCode(systemLanguage);
					}
					SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
					String currentKeyboardLanguage = sharedPreferences.getString(getString(R.string.keyboard_current_language), getString(R.string.default_keyboard_language));
					if(!newUserLanguage.equals(currentKeyboardLanguage)) {
						Editor editor = sharedPreferences.edit();
						editor.putString(getString(R.string.keyboard_current_language), newUserLanguage);
						editor.apply();
					}
					return true;
				}
			});
            
            final Preference suggestionsFuncPref = findPreference(getString(R.string.prefer_suggestions_functionality));        
            suggestionsFuncPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

				@Override
	    		public boolean onPreferenceChange(Preference preference, Object newValue) {
	    			boolean suggestionsOn = (Boolean)newValue;
	    			switchSuggestionsRelatedPreferences(suggestionsOn);
	    			return true;
	    		}
            });

            SeekBarDialogPreference multiKeySensivity = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_multikey_sensitivy));
            multiKeySensivity.setSeekBarRange(-5, 5);
            multiKeySensivity.setSeekBarDescription(getString(R.string.prefer_multikey_sensitivy_seek_bar_desc));
            multiKeySensivity.setSeekBarDefaultValue(0);
            
            int displayHeight = getDisplayHeightInPortrait();
            int displayWidth = getDisplayWidthInPortrait();
            
            SeekBarDialogPreference portraitKeyHeight = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_portrait_key_height));
            portraitKeyHeight.setSeekBarRange(displayHeight/20, displayHeight/10);
            portraitKeyHeight.setSeekBarDescription(getString(R.string.prefer_key_height_desc));
            portraitKeyHeight.setSeekBarDefaultValue((int)getResources().getDimension(R.dimen.key_height));
            
            SeekBarDialogPreference landscapeKeyHeight = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_landscape_key_height));
            landscapeKeyHeight.setSeekBarRange(displayWidth/12, displayWidth/7);
            landscapeKeyHeight.setSeekBarDescription(getString(R.string.prefer_key_height_desc));
            landscapeKeyHeight.setSeekBarDefaultValue((int)getResources().getDimension(R.dimen.key_height_landscape));
            
            SeekBarDialogPreference portraitMultiKeyHeight = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_portrait_multikey_height));
            portraitMultiKeyHeight.setSeekBarRange(displayHeight/10, displayHeight/6);
            portraitMultiKeyHeight.setSeekBarDescription(getString(R.string.prefer_multikey_height_desc));
//            portraitMultiKeyHeight.setSeekBarDefaultValue((int)getResources().getDimension(R.dimen.multi_key_height));
            portraitMultiKeyHeight.setSeekBarDefaultValue(Math.min(displayHeight/6, (int)getResources().getDimension(R.dimen.multi_key_height)));
//            
            
            SeekBarDialogPreference landscapeMultiKeyHeight = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_landscape_multikey_height));
            landscapeMultiKeyHeight.setSeekBarRange(displayWidth/6, 1*displayWidth/3);
            landscapeMultiKeyHeight.setSeekBarDescription(getString(R.string.prefer_multikey_height_desc));
            landscapeMultiKeyHeight.setSeekBarDefaultValue((int)getResources().getDimension(R.dimen.multi_key_height));
            
            SeekBarDialogPreference portraitBottomPadding = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_portrait_botton_padding_height));
            portraitBottomPadding.setSeekBarRange(0, 100);
            portraitBottomPadding.setSeekBarDescription(getString(R.string.prefer_botton_padding_height_desc));
            portraitBottomPadding.setSeekBarDefaultValue(0);
            
            SeekBarDialogPreference landscapeBottomPadding = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_landscape_botton_padding_height));
            landscapeBottomPadding.setSeekBarRange(0, 100);
            landscapeBottomPadding.setSeekBarDescription(getString(R.string.prefer_botton_padding_height_desc));
            landscapeBottomPadding.setSeekBarDefaultValue(0);
            
            SeekBarDialogPreference candidateViewHeight = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_suggestions_candidates_dimens_total_height));
            candidateViewHeight.setSeekBarRange(40, 100);
            candidateViewHeight.setSeekBarDescription(getString(R.string.prefer_suggestions_candidates_dimens_total_height_desc));
            candidateViewHeight.setSeekBarDefaultValue((int)(getResources().getDimension(R.dimen.candidate_font_height) + getResources().getDimensionPixelSize(R.dimen.candidate_vertical_padding)));
             
            SeekBarDialogPreference candidateFontSize = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_suggestions_candidates_dimens_font_size));
            candidateFontSize.setSeekBarRange(70, 100);
            candidateFontSize.setSeekBarDescription(getString(R.string.prefer_suggestions_candidates_dimens_font_size_desc));
            candidateFontSize.setSeekBarDefaultValue(getResources().getInteger(R.integer.candidate_font_height_percent));
            
            SeekBarDialogPreference candidatesAmount = (SeekBarDialogPreference) findPreference(getString(R.string.prefer_suggestions_candidates_amount));
            candidatesAmount.setSeekBarRange(1, 30);
            candidatesAmount.setSeekBarDescription(getString(R.string.prefer_suggestions_candidates_amount_desc));
            candidatesAmount.setSeekBarDefaultValue(getResources().getInteger(R.integer.candidates_amount));
            
            final Preference resetGlobal = findPreference(getString(R.string.prefer_reset_global));
            resetGlobal.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), PreferenceDialogWindowActivity.class);
					startActivityForResult(intent, RESET_GLOBAL_REQ);
					return true;
				}
			});
            
            final Preference resetCurrentSuggestionsPreference = findPreference(getString(R.string.prefer_reset_current_suggestions));
            resetCurrentSuggestionsPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), PreferenceDialogWindowActivity.class);
					startActivityForResult(intent, RESET_CURRENT_SUGGESTIONS_REQ);
					return true;
				}
			});
            
            final Preference resetAllSuggestionsPreference = findPreference(getString(R.string.prefer_reset_all_suggestions));
            resetAllSuggestionsPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), PreferenceDialogWindowActivity.class);
					startActivityForResult(intent, RESET_ALL_SUGGESTIONS_REQ);
					return true;
				}
			});
            
            final Preference termsInformationPreference = findPreference(getString(R.string.prefer_terms));
            termsInformationPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), AboutInformationWindow.class);
					intent.putExtra(SoftKeyboardHelper.ABOUT_INFORMATION_TYPE, SoftKeyboardHelper.ABOUT_INFORMATION_TERMS);
					startActivity(intent);
					return true;
				}
			});
            
            final Preference creditsInformationPreference = findPreference(getString(R.string.prefer_credits));
            creditsInformationPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), AboutInformationWindow.class);
					intent.putExtra(SoftKeyboardHelper.ABOUT_INFORMATION_TYPE, SoftKeyboardHelper.ABOUT_INFORMATION_CREDITS);
					startActivity(intent);
					return true;
				}
			});
            
            final Preference tutorialPreference = findPreference(getString(R.string.tutorial_dialog));
            tutorialPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(getActivity(), TutorialDialogWindow.class);
					startActivity(intent);
					return true;
				}
			});
        }
        
		@Override
        public void onActivityResult(int requestCode, int resultCode, Intent intentData) {  
        	super.onActivityResult(requestCode, resultCode, intentData);  

        	if(requestCode == RESET_GLOBAL_REQ) {
        		if(resultCode == RESET_POSITIVE_RESULT_CODE) {
        			final Preference resetGlobal = findPreference(getString(R.string.prefer_reset_global));
                	Editor editor = resetGlobal.getEditor();
        			editor.putLong(getString(R.string.global_last_reset_time), System.currentTimeMillis());
        			editor.apply();
        		}
        		
        	} else if(requestCode == RESET_CURRENT_SUGGESTIONS_REQ) {
        		if(resultCode == RESET_POSITIVE_RESULT_CODE) {
        			final Preference resetCurrentSuggestionsPreference = findPreference(getString(R.string.prefer_reset_current_suggestions));
                	Editor editor = resetCurrentSuggestionsPreference.getEditor();
        			editor.putLong(getString(R.string.current_suggestions_last_reset_time), System.currentTimeMillis());
        			editor.apply();
        		}
        		
        	} else if(requestCode == RESET_ALL_SUGGESTIONS_REQ) {
        		if(resultCode == RESET_POSITIVE_RESULT_CODE) {
        			final Preference resetAllSuggestionsPreference = findPreference(getString(R.string.prefer_reset_all_suggestions));
            		Editor editor = resetAllSuggestionsPreference.getEditor();
    				editor.putLong(getString(R.string.all_suggestions_last_reset_time), System.currentTimeMillis());
    				editor.apply();
        		}
        	}
        }

		private int getDisplayHeightInPortrait() {
        	int height = getResources().getDisplayMetrics().heightPixels;
        	int width = getResources().getDisplayMetrics().widthPixels;
        	return height > width ? height : width;
        }
        
		private int getDisplayWidthInPortrait() {
        	int height = getResources().getDisplayMetrics().heightPixels;
        	int width = getResources().getDisplayMetrics().widthPixels;
        	return height > width ? width : height;
        }
        
		@Override
		public void onStart() {
            super.onStart();
            mSharedPreferences = getPreferenceScreen().getSharedPreferences();
            mSharedPreferences.registerOnSharedPreferenceChangeListener(mImePreference);
        }
        
		@Override
		public void onResume() {
            super.onResume();
            
			final SwitchPreference suggestionsFunctionalityCheckBoxPref = (SwitchPreference) findPreference(getString(R.string.prefer_suggestions_functionality));
			switchSuggestionsRelatedPreferences(suggestionsFunctionalityCheckBoxPref.isChecked());
        }
        
        @Override
    	public void onStop() {
        	super.onStop();
    		if(mSharedPreferences != null) {
    			mSharedPreferences.unregisterOnSharedPreferenceChangeListener(mImePreference);
    		}
    	}
        
		private void switchSuggestionsRelatedPreferences(boolean enabled) {
			final Preference suggestionsDepthPref = findPreference(getString(R.string.prefer_suggestions_min_depth));
			final Preference suggestionsCandidatesAmount = findPreference(getString(R.string.prefer_suggestions_candidates_amount));
			final Preference suggestionsAdvancedPreferenceScreen = findPreference(getString(R.string.prefer_suggestions_advanced_screen));

			suggestionsCandidatesAmount.setEnabled(enabled);
			suggestionsDepthPref.setEnabled(enabled);
			suggestionsAdvancedPreferenceScreen.setEnabled(enabled);
		}
    }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String sharedpreferenceKey) {
		if(sharedpreferenceKey.equals(getString(R.string.keyboard_current_language))) {
			this.recreate();
		} else if(sharedpreferenceKey.equals(getString(R.string.global_last_reset_time))) {
			this.recreate();
		}
	}
}
