package com.gigantbuttonskeyboard.preferences;

import com.gigantbuttonskeyboard.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SeekBarDialogPreference extends DialogPreference {

	private int mMinValue;
	private int mCurrentValue;
	private int mMaxValue;
	private int mDefaultValue;
	private String mSeekBarDescriotion;
	
	SeekBar mSeekBar;
	
	public SeekBarDialogPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		setPersistent(true);
		mMinValue = 0;
	    mMaxValue = 10;
	    mSeekBarDescriotion = "";
	    mDefaultValue = mMinValue;
	}
	
	@Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
		super.onPrepareDialogBuilder(builder);  
		builder.setPositiveButton("OK", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				persistInt(mCurrentValue + mMinValue);
			}
		});
	}
	
	@Override
	public void onBindDialogView(View view) {
		super.onBindDialogView(view);
		
		int valueFromSharedPreferences = getPersistedInt(mDefaultValue);

		final TextView currentValueTextView = (TextView) view.findViewById(R.id.current_info_text_view);
		currentValueTextView.setText(String.valueOf(valueFromSharedPreferences));
		setCurrentValue(valueFromSharedPreferences - mMinValue);
		
		mSeekBar = (SeekBar) view.findViewById(R.id.preference_seek_bar);
		mSeekBar.setMax(mMaxValue - mMinValue);
		mSeekBar.setProgress(valueFromSharedPreferences - mMinValue);

		final TextView minValueTextView = (TextView) view.findViewById(R.id.min_info_text_view);
		minValueTextView.setText(String.valueOf(mMinValue));
		final TextView maxValueTextView = (TextView) view.findViewById(R.id.max_info_text_view);
		maxValueTextView.setText(String.valueOf(mSeekBar.getMax() + mMinValue));
		
		mSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				setCurrentValue(seekBar.getProgress());
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				currentValueTextView.setText(String.valueOf(progress + mMinValue));
			}
		});
		
		Button resetToDefaults = (Button) view.findViewById(R.id.seek_bar_reset_value_button);
		resetToDefaults.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				currentValueTextView.setText(String.valueOf(mDefaultValue));
				mSeekBar.setProgress(mDefaultValue - mMinValue);
				setCurrentValue(mDefaultValue - mMinValue);
			}
		});
		
		TextView seekBarDescriptionTextView = (TextView) view.findViewById(R.id.description_text_view);
		seekBarDescriptionTextView.setText(mSeekBarDescriotion);
	}
	
	private void setCurrentValue(int value) {
		mCurrentValue = value;
	}
	
	public void setSeekBarRange(int min, int max) {
		mMinValue = min;
		mMaxValue = max;
	}
	
	public void setSeekBarDescription(String description) {
		mSeekBarDescriotion = description;
	}
	
	public void setSeekBarDefaultValue(int value) {
		mDefaultValue = value;
	}
}
