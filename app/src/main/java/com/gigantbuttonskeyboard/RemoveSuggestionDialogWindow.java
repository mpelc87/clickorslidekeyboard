package com.gigantbuttonskeyboard;

import com.gigantbuttonskeyboard.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class RemoveSuggestionDialogWindow extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Intent intent = getIntent();
	    final String suggestion = intent.getStringExtra("suggestion");
	    
	    AlertDialog.Builder builder = initBuilder(suggestion);
	
	    AlertDialog alertDialog = builder.create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				SoftKeyboard.Companion.getSoftKeyboard().returnToComposingIfAppropriate(); //FIXME: Workaround for duplicating words while dialog in Android > 5.0
				dialog.cancel();
                finish();
			}
		});
	    alertDialog.show();
	}

	private AlertDialog.Builder initBuilder(final String suggestion) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(String.format(getResources().getString(R.string.remove_suggestion_dialog_message), suggestion.toLowerCase()))
				.setPositiveButton(R.string.default_dialog_positive, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int id) {
				        SoftKeyboard.Companion.getSoftKeyboard().removeSuggestion(suggestion);
				        dialog.dismiss();
				    }
				})
				.setNegativeButton(R.string.default_dialog_negative, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int id) {
				    	SoftKeyboard.Companion.getSoftKeyboard().updateCandidates();
				    	dialog.dismiss();
				    }
				});
		return builder;
	}

}