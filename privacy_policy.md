TERMS AND CONDITIONS

PRIVACY POLICY

The application does NOT transmit typed words anywhere off the device by any medium.
The application stores ONLY words EXPLICITLY added to dictionary by user. Other words are NOT stored.
The user should be careful not to store any words which might be passwords into dictionary because it will be later visible in suggestions.

LIMITATION OF LIABILITY

The author is not liable for the consequences of using this application by user.

PERMISSIONS

- INTERNET  - Application ask for internet access in order to make google ads to work.

- ACCESS_NETWORK_STATE - Application ask for internet access in order to make google ads to work.

- BIND_INPUT_METHOD - needed for this app to server its primary function which is software keyboard.


CONTACT US

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
Contact Information:
Email: mpelc87@gmail.com